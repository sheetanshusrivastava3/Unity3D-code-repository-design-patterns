/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Connected Experiences, Inc.
==============================================================================*/

using UnityEngine;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class DefaultTrackableEventHandler : MonoBehaviour,
                                            ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
	private bool mHasBeenFound = false;
	private bool mLostTracking;
	private float mSecondsSinceLost;

    #endregion // PRIVATE_MEMBER_VARIABLES





    #region UNTIY_MONOBEHAVIOUR_METHODS
    
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

		OnTrackingLost();
    }


	void Update()
	{
		if(AppManagerMore.instance.isVideoPlayingWhileScanning)
		{
#if UNITY_ANDROID
			// Pause the video if tracking is lost for more than two seconds
			if (mHasBeenFound && mLostTracking)
			{
				if (mSecondsSinceLost > 2.0f)
				{
					VideoPlaybackBehaviour video = GetComponentInChildren<VideoPlaybackBehaviour>();
					if (video != null &&
					    video.CurrentState == VideoPlayerHelper.MediaState.PLAYING)
					{
						video.VideoPlayer.Pause();
					}
					
					mLostTracking = false;
				}
				
				mSecondsSinceLost += Time.deltaTime;
			}
#endif
		}
	}


    #endregion // UNTIY_MONOBEHAVIOUR_METHODS



    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
		if(AppManagerMore.instance.canTargetNowBeScanned)
		{
			AppManagerMore.instance.showLoaderAndCloseButton(false);
			if(AppManagerMore.instance.toShowPlayButton)
			{
				AppManagerMore.instance.playButtonWhileScanning.SetActive(true);
				AppManagerMore.instance.LoaderWhileScanning.SetActive(false);
				AppManagerMore.instance.videoThumb.SetActive(true);
			}
			else if(AppManagerMore.instance.isVideoPlayingWhileScanning)
			{
				AppManagerMore.instance.videoThumb.SetActive(true);
#if UNITY_ANDROID
				VideoPlaybackBehaviour video = GetComponentInChildren<VideoPlaybackBehaviour>();
				if (video != null && video.AutoPlay)
				{
					if (video.VideoPlayer.IsPlayableOnTexture())
					{
						VideoPlayerHelper.MediaState state = video.VideoPlayer.GetStatus();
						if (state == VideoPlayerHelper.MediaState.PAUSED ||
						    state == VideoPlayerHelper.MediaState.READY ||
						    state == VideoPlayerHelper.MediaState.STOPPED)
						{
							// Pause other videos before playing this one
							PauseOtherVideos(video);
							
							// Play this video on texture where it left off
							video.VideoPlayer.Play(false, video.VideoPlayer.GetCurrentPosition());
						}
						else if (state == VideoPlayerHelper.MediaState.REACHED_END)
						{
							// Pause other videos before playing this one
							PauseOtherVideos(video);
							
							// Play this video from the beginning
							video.VideoPlayer.Play(false, 0);
						}
					}
				}
				
				mHasBeenFound = true;
				mLostTracking = false;
#endif
#if UNITY_IOS
				if(AppManagerMore.instance.toResumeVideoiOS)
				{
					AppManagerMore.instance.playIconiOS.SetActive(false);
					AppManagerMore.instance.video.Resume ();
					AppManagerMore.instance.hasVideoEnded = false;
				}
#endif
			}
			else
			{
				AppManagerMore.instance.playButtonWhileScanning.SetActive(false);
				AppManagerMore.instance.videoThumb.SetActive(false);
				AppManagerMore.instance.LoaderWhileScanning.SetActive(true);
			}
//	        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
//	        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
//
//	        // Enable rendering:
//	        foreach (Renderer component in rendererComponents)
//	        {
//	            component.enabled = true;
//	        }
//
//	        // Enable colliders:
//	        foreach (Collider component in colliderComponents)
//	        {
//	            component.enabled = true;
//	        }
			AppManagerMore.instance.isTargetScannedOnce = true;
		}
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

    }


    private void OnTrackingLost()
    {
		if(AppManagerMore.instance.canTargetNowBeScanned)
		{
			if(AppManagerMore.instance.isTargetScannedOnce)
			{
//		        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
//		        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
//
//		        // Disable rendering:
//		        foreach (Renderer component in rendererComponents)
//		        {
//		            component.enabled = false;
//		        }
//
//		        // Disable colliders:
//		        foreach (Collider component in colliderComponents)
//		        {
//		            component.enabled = false;
//		        }

				if(AppManagerMore.instance.isVideoPlayingWhileScanning)
				{
#if UNITY_ANDROID
					mLostTracking = true;
					mSecondsSinceLost = 0;

#endif
#if UNITY_IOS
					if(AppManagerMore.instance.playIconiOS.activeInHierarchy == false)
					{
						AppManagerMore.instance.video.Pause();
						AppManagerMore.instance.toResumeVideoiOS = true;
						AppManagerMore.instance.playIconiOS.SetActive(true);
					}
#endif
				}

				if(AppManagerMore.instance.hasVideoEnded)
				{
					#if UNITY_IOS
					AppManagerMore.instance.video.Stop();
					#endif
					AppManagerMore.instance.loadRecognitionScene();
				}
				else
				{
					AppManagerMore.instance.showLoaderAndCloseButton(true);
				}

				AppManagerMore.instance.playButtonWhileScanning.SetActive(false);
				AppManagerMore.instance.LoaderWhileScanning.SetActive(false);
				AppManagerMore.instance.videoThumb.SetActive(false);
			}
		}
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");

    }


	// Pause all videos except this one
	private void PauseOtherVideos(VideoPlaybackBehaviour currentVideo)
	{
		VideoPlaybackBehaviour[] videos = (VideoPlaybackBehaviour[])
			FindObjectsOfType(typeof(VideoPlaybackBehaviour));
		
		foreach (VideoPlaybackBehaviour video in videos)
		{
			if (video != currentVideo)
			{
				if (video.CurrentState == VideoPlayerHelper.MediaState.PLAYING)
				{
					video.VideoPlayer.Pause();
				}
			}
		}
	}


    #endregion // PRIVATE_METHODS
}
