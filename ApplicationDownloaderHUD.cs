﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using System;
using ConfigurationLibrary;

/// <summary>
///  ApplicationDownloaderHUD : : Manages all the functions and events for Initial Downloading and Caching
/// </summary>
public class ApplicationDownloaderHUD : MonoBehaviour
{	
	
	// Variable declarations for test cases
	[SerializeField]	private TextAsset diagnosticComponentFile;						// File from which diagnostic Json is retrived(only for test case)
	[SerializeField]	private TextAsset educationalComponentFile;						// File from which educational Json is retrived(only for test case)
	
	[SerializeField]	private ApplicationManager applicationManager;
	
	//[SerializeField]	private GameObject boySprite;
	//[SerializeField]	private GameObject girlSprite;
	[SerializeField]	private GameObject loaderSprite;
	
	//[SerializeField]	private Vector3 leftPos;
	//[SerializeField]	private Vector3 rightPos;
	
	[SerializeField]	private UILabel headerLabel;
	
	[SerializeField]	private GameObject panelPopUP;
	[SerializeField]	private UISprite loadingBarSprite;	

	private string currentImageURL;
	private string currentImageName;
	
	private string jsonDownloadingText = "Downloading Kingdom Data";
	private string dataDownloadingText = "Downloading Kingdom Images";
	private string checkingUpdateText = "Checking For New Updates";
	private int imageCount = 0;
	private int currentImageCount = 0;
	private WWW imageWWW;

	public enum AnimationMode
	{
		LTR,
		RTL
	}
	
	private AnimationMode boyMode;
	private AnimationMode girlMode;
	
	private bool toShowProgess = false;
		
	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{	
		loadingBarSprite.fillAmount = 0f;
		imageCount= 0;
		currentImageCount = 0;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		applicationManager.soundControllerScript.playchainDropScreenSound();
		Constants.BeginTweenPosition(panelPopUP,Constants.panelDownTime,Constants.animatedPanelActualPos,Constants.animatedPanelTopPos,UITweener.Method.BounceIn,gameObject,"PanelDownAnimationComplete");
//		Constants.BeginTweenPosition(boySprite,5.0f,rightPos,leftPos,UITweener.Method.Linear,gameObject,"BoyAnimComplete");
//		Constants.BeginTweenPosition(girlSprite,5.0f,leftPos,rightPos,UITweener.Method.Linear,gameObject,"GirlAnimComplete");
	}

	void Update()
	{
		if(toShowProgess)
		{
			//Debug.Log("www progress image : " +(imageWWW.progress));
			loadingBarSprite.fillAmount = ((float)currentImageCount/(float)imageCount) + ((1f/(float)imageCount)*(imageWWW.progress));
		}
	}

	IEnumerator CheckState()
	{
		// If he is coming to this screen even though the data is cached means the user is downloading an update so reallow him to cache data
		if(PlayerPrefsManager.JsonCached && PlayerPrefsManager.DataCached)
		{
			Debug.Log("New Update is available lets try data download");
			PlayerPrefsManager.JsonCached = false;
			PlayerPrefsManager.DataCached = false;
		}
		
		loaderSprite.gameObject.SetActive(true);
		if(!PlayerPrefsManager.JsonCached)
		{
			headerLabel.text = jsonDownloadingText;
			yield return StartCoroutine("GetJsonFile");	
		}
		
		else if(!PlayerPrefsManager.DataCached)
		{
			headerLabel.text = dataDownloadingText;
			yield return StartCoroutine("ParseJsonAndCacheData");
			Debug.Log("Start Animation");
			Constants.BeginTweenPosition(panelPopUP,Constants.panelUpTime,Constants.animatedPanelTopPos,Constants.animatedPanelActualPos,UITweener.Method.Linear,gameObject,"PanelUpAnimationComplete");
		}
	}
	
	void PanelUpAnimationComplete()
	{
		Debug.Log("Animation Complete");
		applicationManager.StartApplication();
	}
	
	void PanelDownAnimationComplete()
	{
		StartCoroutine("CheckState");
	}
	
	IEnumerator GetJsonFile()
	{
		// Get New Json through WWW call from the server
		yield return new WaitForSeconds(1.0f);
		if(applicationManager.toLoadFromOldJson)
		{
			Constants.WriteDataToFile(CryptoString.Encrypt(diagnosticComponentFile.text),Constants.diagnosticComponentFileName);
			Constants.WriteDataToFile(CryptoString.Encrypt(educationalComponentFile.text),Constants.educationalComponentFileName);
			StartCoroutine(cacheDownloadedData());
		}
		else
		{
			StartCoroutine(GetEducationalJsonFromWeb());
		}
	}


	IEnumerator GetEducationalJsonFromWeb()
	{
		WWW www = new WWW(Constants.educationalJsonURL);
		yield return www;

		if(www.error == null)
		{
			if(www.text != null)
			{
				Constants.WriteDataToFile(CryptoString.Encrypt(www.text),Constants.educationalComponentFileName);
				StartCoroutine(GetDiagnosticJsonFromWeb());
			}
			else
			{
				Debug.Log("Error downloading educational data");
				applicationManager.showDataNotProperlyDownloadedPopUp();
			}
		}
		else
		{
			Debug.Log("Error downloading educational data");
			applicationManager.showDataNotProperlyDownloadedPopUp();
		}


	}


	IEnumerator GetDiagnosticJsonFromWeb()
	{
		WWW www = new WWW(Constants.diagnosticJsonURL);
		yield return www;

		if(www.error == null)
		{
			if(www.text != null)
			{
				Constants.WriteDataToFile(CryptoString.Encrypt(www.text),Constants.diagnosticComponentFileName);
				StartCoroutine(cacheDownloadedData());
			}
			else
			{
				Debug.Log("Error downloading diagnostic data");
				applicationManager.showDataNotProperlyDownloadedPopUp();
			}
		}
		else
		{
			Debug.Log("Error downloading diagnostic data");
			applicationManager.showDataNotProperlyDownloadedPopUp();
		}

	}


	IEnumerator cacheDownloadedData()
	{
		PlayerPrefsManager.JsonCached = true;
		
		if(!PlayerPrefsManager.DataCached)
		{
			headerLabel.text = dataDownloadingText;
			yield return StartCoroutine("ParseJsonAndCacheData");
			Debug.Log("Start Animation");
			//applicationManager.soundControllerScript.playchainDropScreenSound();
			Constants.BeginTweenPosition(panelPopUP,Constants.panelUpTime,Constants.animatedPanelTopPos,Constants.animatedPanelActualPos,UITweener.Method.Linear,gameObject,"PanelUpAnimationComplete");
		}
	}

	
	
	/// <summary>
	/// Parses the json and caches data.
	/// </summary>
	/// <returns>
	/// The json and cache data.
	/// </returns>
	IEnumerator ParseJsonAndCacheData()
	{									
		EducationalData educationData = JsonFx.Json.JsonReader.Deserialize<EducationalData>(CryptoString.Decrypt(Constants.ReadDataFromFile(Constants.educationalComponentFileName)));

		Debug.Log("imageCount : " + educationData.imageCount);
		imageCount = educationData.imageCount;

		foreach(ClassLevelData classData in educationData.classData)
		{
			// Getting image URL for Main Concept Example
			currentImageURL = classData.mainConceptExample.imageURL;
			currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

			if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
			{
				Debug.Log("image Exists");
				increaseImageCount();
			}

			if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
				yield return StartCoroutine("DownloadImageAndCache");
			else
				Debug.Log("Image ALready Exists or Null Value  "+currentImageName);
			
			// Getting image URL for Detail Example
			currentImageURL = classData.detailsExample.imageURL;
			currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

			if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
			{
				Debug.Log("image Exists");
				increaseImageCount();
			}

			if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
				yield return StartCoroutine("DownloadImageAndCache");
			else
				Debug.Log("Image ALready Exists or Null Value");
			
			foreach(Levels level in classData.level)
			{
				if(level.topics != null)
				{
					foreach(Topics topic in level.topics)
					{	
						foreach(EducationalMainConcept mainConcept in topic.mainConcept)
						{
							currentImageURL = mainConcept.imageURL;
							currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

							if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
							{
								Debug.Log("image Exists");
								increaseImageCount();
							}

							if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
								yield return StartCoroutine("DownloadImageAndCache");
							else
								Debug.Log("Image ALready Exists or Null Value");
						}
						
						foreach(EducationalDetails detail in topic.details)
						{
							currentImageURL = detail.imageURL;
							currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

							if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
							{
								Debug.Log("image Exists");
								increaseImageCount();
							}

							if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
								yield return StartCoroutine("DownloadImageAndCache");
							else
								Debug.Log("Image ALready Exists or Null Value");
						}
					}
				}
			}
		}
		
		if(educationData.readingData != null)
		{
			foreach(ReadingLevelData classData in educationData.readingData)
			{				
				foreach(Levels level in classData.level)
				{
					if(level.topics != null)
					{
						foreach(Topics topic in level.topics)
						{	
							foreach(EducationalMainConcept mainConcept in topic.mainConcept)
							{
								currentImageURL = mainConcept.imageURL;
								currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

								if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
								{
									Debug.Log("image Exists");
									increaseImageCount();
								}

								if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
									yield return StartCoroutine("DownloadImageAndCache");
								else
									Debug.Log("Image ALready Exists or Null Value");
							}
							
							foreach(EducationalDetails detail in topic.details)
							{
								currentImageURL = detail.imageURL;
								currentImageName = Path.GetFileNameWithoutExtension(currentImageURL);

								if(Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
								{
									Debug.Log("image Exists");
									increaseImageCount();
								}

								if(!Constants.DoesFileExist(currentImageName) && !string.IsNullOrEmpty(currentImageName))
									yield return StartCoroutine("DownloadImageAndCache");
								else
									Debug.Log("Image ALready Exists or Null Value");
							}
						}
					}
				}
			}
		}
		
		Debug.Log("Download Complete");
		PlayerPrefsManager.DataCached = true;
		educationData = null;
	}
	
	/// <summary>
	/// Downloads the image and cache it
	/// </summary>
	/// <returns>
	/// The image and cache.
	/// </returns>
	IEnumerator	DownloadImageAndCache()
	{
		imageWWW = new WWW(currentImageURL.Replace(" ","%20"));
		Debug.Log(currentImageURL);
		toShowProgess = true;
		yield return imageWWW;
		toShowProgess = false;
		
		if(imageWWW.error == null)
		{
			if(imageWWW.texture != null)
			{
				Byte[] bytesToEncode = imageWWW.texture.EncodeToPNG();			
				Constants.WriteDataToFile(Convert.ToBase64String(bytesToEncode),currentImageName);
			}
			else
			{
				Debug.Log("Error downloading image : " + currentImageName);
			}
		}
		else
		{
			Debug.Log("Error in Image " + imageWWW.error+ "  "+currentImageName);
		}

		increaseImageCount();
	}


	public void increaseImageCount()
	{
		currentImageCount++;
		loadingBarSprite.fillAmount = ((float)currentImageCount/(float)imageCount);
	}

//	void OnDisable()
//	{	
//		Screen.sleepTimeout = 60;
//	}
			
//	/// <summary>
//	/// Starts the boy girl animation.
//	/// </summary>
//	void EnableBoyGirlAnimation(bool state)
//	{	
//		Debug.Log("Here");
//		//loaderSprite.gameObject.SetActive(state);		
//		boySprite.GetComponent<SpriteRenderer>().enabled = state;
//		girlSprite.GetComponent<SpriteRenderer>().enabled = state;
//	}
//	
//	/// <summary>
//	/// Invoke on Boy animation complete.
//	/// </summary>
//	void BoyAnimComplete()
//	{
//		boySprite.transform.localScale = new Vector3((-1) * boySprite.transform.localScale.x,boySprite.transform.localScale.y,boySprite.transform.localScale.z);
//		boyMode = boyMode == AnimationMode.LTR ? AnimationMode.RTL : AnimationMode.LTR;
//		Constants.BeginTweenPosition(boySprite,5.0f, (boyMode == AnimationMode.LTR ? rightPos :leftPos) , (boyMode == AnimationMode.LTR ? leftPos :rightPos) ,UITweener.Method.Linear,gameObject,"BoyAnimComplete");
//	}
//	
//	/// <summary>
//	/// Invoke on Girl animation complete.
//	/// </summary>
//	void GirlAnimComplete()
//	{
//		girlSprite.transform.localScale = new Vector3((-1) * girlSprite.transform.localScale.x,girlSprite.transform.localScale.y,girlSprite.transform.localScale.z);
//		girlMode = girlMode == AnimationMode.LTR ? AnimationMode.RTL : AnimationMode.LTR;
//		Constants.BeginTweenPosition(girlSprite,5.0f, (girlMode == AnimationMode.LTR ? rightPos :leftPos) , (girlMode == AnimationMode.LTR ? leftPos :rightPos) ,UITweener.Method.Linear,gameObject,"GirlAnimComplete");
//	}
}
