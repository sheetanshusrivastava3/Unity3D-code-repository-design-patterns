using UnityEngine;
using System.Collections;

public class NetMatch : MonoBehaviour {
	
	private int m_lastTeamScore;
	private int m_lastLocalScore;
	
	private bool m_matchOver;
	
	//START SINGLETON
    private static NetMatch m_instance;
   
    public static NetMatch instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = new GameObject ("NetPlayerSingleton").AddComponent<NetMatch> ();
            }

            return m_instance;
        }
    }
   
    public void OnApplicationQuit ()
    {
        m_instance = null;
    }
	
	public static void Release(){
		if( m_instance != null ){
		 	Destroy( m_instance.gameObject );
			m_instance = null;
		}
	}
	
	//END SINGLETON
	
	
	public void Initialize(){
		
	}
	
	void Start()
	{
		if(RoomManager.gameType == NetGameType.CaptureTheFlag)
			SetUpCaptureTheFlag();
	}
	
	// Update is called once per frame
	void Update () {
		if( RoomManager.waitingForPlayers )
			return;
		if( RoomManager.state == NetRoomState.InMatch ){
			NetGameType gameType = RoomManager.gameType;
			if( gameType != NetGameType.Unresolved ){
			
				switch( gameType ){
				case NetGameType.FreeForAll:
					UpdateFreeForAll();
					break;
				case NetGameType.CaptureTheFlag:
					UpdateCaptureTheFlag();
					break;
				}
	
			}
		}
		if( PhotonNetwork.isMasterClient )
		{
			if(Time.timeSinceLevelLoad >= (float)PhotonNetwork.room.customProperties["TimeLimit"])
			{
				Debug.Log("Time limit reached, match over!");
				VictoryConditionMet();
			}
		}
	}
	
	//place flags, flag capture zones and set spawn points for each team.
	private void SetUpCaptureTheFlag()
	{
		TeamManager.SetTeamCount(2);
		TeamManager.SetMinPlayersPerTeam(1);
		TeamManager.SetPlayerTeams();

		GameObject[] flags = GameObject.FindGameObjectsWithTag("flag");
		for(int i = 0; i < flags.Length; i++)
		{
			flags[i].SetActiveRecursively(true);
		}
	}
	
	private void UpdateFreeForAll(){
		
		//Check if the victory condition has been met
		for( int i = 0; i < PhotonNetwork.room.playerCount; i++ ){
			if( PlayerManager.GetKillCount( PhotonNetwork.playerList[i] ) >= RoomManager.scoreLimit ){
				Debug.Log( "Match won!" );
				//Set the FFA Winnder
				RoomManager.FFAWinnerName = PhotonNetwork.playerList[i].name;
				
				//Here we probably would want to submit the winner's victory to the tracking system and such, and all other player stats.
				
				//Game over, winnder decided
				VictoryConditionMet();
				
			}
		}
		
	}
	
	
	private void UpdateCaptureTheFlag()
	{
		
	}
	
	private void VictoryConditionMet(){
		
		if(!m_matchOver)
		{
			//End the match 
			m_matchOver = true;
			
			Debug.Log("Room Manager type : " + RoomManager.gameType);
			//Invoke the victory response for the current 
			switch( RoomManager.gameType ){
			case NetGameType.FreeForAll:
				FreeForAllVictory();
				Screen.showCursor = true;
				Screen.lockCursor = false;
				break;
			case NetGameType.TeamMatch:
				TeamMatchVictory();
				Screen.showCursor = true;
				Screen.lockCursor = false;
				break;
			case NetGameType.CaptureTheFlag:
				CaptureTheFlagVictory();
				Screen.showCursor = true;
				Screen.lockCursor = false;
				break;
			}
		}
		
	}
	
	private void FreeForAllVictory(){
		
		HUD.instance.display = false;
		//End the match, invoke the FFA game summary
		FPSCoordinator.instance.StartEndGameSummary();
		
		//save PP
		
	}

	private void TeamMatchVictory(){
		//End the match, invoke the Team Match game summary
	}
	
	private void CaptureTheFlagVictory(){
		HUD.instance.display = false;
		//End the match, invoke the FFA game summary
		FPSCoordinator.instance.StartEndGameSummary();
	}
	                
	public void PlayerKilled( PhotonPlayer dead, PhotonPlayer killer ){
		
		if( m_matchOver )
			return;
			
		if(killer != null)
		{
			// don't raise points if killed by hismself
			if(killer != PhotonNetwork.player)
			{
				PlayerManager.SetKillCount( killer, PlayerManager.GetKillCount( killer ) +1 );
				PlayerManager.SetPlayerPoints(killer, PlayerManager.GetPlayerPoints(killer) +10);
			}
			
			PlayerManager.SetDeathCount( dead, PlayerManager.GetDeathCount( dead ) +1 );
			
			if(RoomManager.gameType == NetGameType.FreeForAll)
			{
				Debug.Log("We set a players goal score!");
				if(killer != PhotonNetwork.player)
					PlayerManager.SetGoalScore(killer, PlayerManager.GetGoalScore(killer) + 10);
				Debug.Log(PlayerManager.GetGoalScore(killer).ToString());
			}
		}
		else
		{
			//Otherwise we had a self inflicted death, player probably fell from too high, incapacitated
			//here we would want to increment deaths/add a suicide counter
			PlayerManager.SetDeathCount( dead, PlayerManager.GetDeathCount( dead ) +1 );
		}

	}
	
	public void UpdateShotFired(PhotonPlayer player,int shotsFired)
	{
		PlayerManager.SetShotsFired(player,shotsFired);
	}
	
	public void UpdateHeadShotsFired(PhotonPlayer player,int headShotsFired)
	{
		PlayerManager.SetHeadshots(player,headShotsFired);
	}
	
	public void FlagCapture(PhotonPlayer carrier)
	{
		if(m_matchOver)
			return;
		if(carrier != null)
		{
			PlayerManager.SetGoalScore(carrier, PlayerManager.GetGoalScore(carrier));
			PlayerManager.SetPlayerPoints(carrier, PlayerManager.GetPlayerPoints(carrier) +15);
		}
	}
	
	void OnPhotonPlayerConnected(PhotonPlayer carrier)
	{
	}
	
	public bool IsMatchOver
	{
		get
		{
			return m_matchOver;
		}
	}
}
