using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 0414
public class NetPlayer : Photon.MonoBehaviour{
	
	private static List< NetPlayer > m_players;
	
	public static void AddPlayer( NetPlayer player ){
			
		for( int i = m_players.Count-1; i >= 0; i-- ){
			if( m_players[ i ] == null ){
				m_players.RemoveAt( i );
			}
		}
	}
	
	public AudioClip[] m_footStepClips;
	private FootStepAudio m_footStepAudioPlayer;
	
	static NetPlayer(){
		m_players = new List<NetPlayer>();
	}
	
	public static List< NetPlayer > allPlayers{
		get{ return m_players; }
	}
	
	public static NetPlayer Get( Component component ){
		return component.transform.root.GetComponent< NetPlayer >();	
	}
	
	public static void ClearPlayerList(){
		m_players.Clear();
	}
	
	//The photonview for this GO
	private PhotonView m_photonView;
	private PhotonPlayer m_player;
	
	public GunAnimController 	m_gunAnimController;
	public BodyAnimController	m_bodyAnimController;
	private Weapon				m_weapons;
	public GameObject[]			m_tubes;
	public AudioClip			m_countdownClip;
	public AudioClip			m_startGameClip;
	public AudioClip[] 			highFallSound;
	public UILabel              reloadLabel;
	
	[SerializeField] private Material[] tps_helmetMaterials;
	[SerializeField] private Material[] tps_bodyMaterials;
	[SerializeField] private Material[] tps_maskMaterials;
	[SerializeField] private Material[] fps_bodyMaterials;
	
	[SerializeField] private GameObject tps_helmet;
	[SerializeField] private GameObject tps_body;
	[SerializeField] private GameObject tps_mask;
	[SerializeField] private GameObject fps_body;
	
	private int jumpThreshold = 6;
	private int jumpHeight = 0;
	private bool checkForJumpDamage;
	private bool grounded = false;
	private RaycastHit hit;
	
	//property
	private CharacterController Controller { get { return gameObject.GetComponent<CharacterController>(); } }
	
	SimpleScript m_Controller;
		
	public PhotonPlayer player {
		get {
			return this.m_player;
		}
		set {
			m_player = value;
		}
	}
	
	public bool IsViewMine
	{
		get
		{
			return m_photonView.isMine;
		}
	}

	private bool m_trafficCalculated;
	
	public GUISkin m_skin;
	
	//Hit points
	public float m_hp = 100;
	
	//Death
	public bool m_dead = true, m_firstSpawn = true;
	public int m_deadTime =0;//When did I die?
	
	//Visibility
	private bool m_isVisible = true, m_showNextFrame = false;
	
	//Interpolation
	public double m_InterpolationBackTime = 0.1;
	public double m_ExtrapolationLimit = 0.5;
	
	//Leaning
	public bool 			leanBlockedLeft{get;set;}
	public bool 			leanBlockedRight{get;set;}
	private bool 			m_leanLeft = false, m_leanRight = false;
	public float 			m_maxLeanAngle, m_leanRate;
	private float 			m_lean;
    static public bool      m_leaning = false;
	
	//stance
	private float 			m_crouchKeyTime;
	private bool			m_crouching, m_prone;//Are we crouching
	private float 			m_stance;//How much are we crouched/prone
	private float 			m_maxCrouch = -.8f;
	private float 			m_maxProne = -1.3f;
	public float			m_stanceRate = 5;
	public bool				m_prevProne, m_prevCrouch, m_prevStand;
	private bool			m_carryingFlag;
	private bool			m_prevCarryingFlag;
	private GameObject		m_flagToCarry;
	private bool 			m_canShowGrenadeEffect;
	
	private int				grenadeCount;
	private int 			smokeGrenadeCount;
	
	//Aiming
	private bool			m_isAiming;
	
	//Weapons
	private GunType			m_prevGunType;

	//Sounds for standing/crouching
	public GameObject m_runSoundObject, m_walkSoundObject;
	public float lean {
		get {
			return this.m_lean;
		}
		set {
			m_lean = value;
		}
	}
	
	public GameObject flagToCarry{
		get{ return m_flagToCarry; }
		set{ m_flagToCarry = value; }
	}
	
	public bool carryingFlag{
		get{ return m_carryingFlag; }
		set{ m_carryingFlag = value; }
	}
	
	public float stance{
		get{ return m_stance; }	
	}

	public bool crouching{
		get{ return m_crouching; }	
		set{ m_crouching = value; }
	}

	public bool prone{
		get{ return m_prone; }
		set{ m_prone = value; }
	}
	
	public bool standing{
		get{ return !m_prone && !m_crouching; }	
	}
	
	public bool startedProne{
		get{ return m_prone != m_prevProne && m_prone; }
	}
	
	public bool startedCrouch{
		get{ return m_crouching != m_prevCrouch && m_crouching; }
	}
	
	public bool startedStanding{
		get{ return	 (!m_prone&&!m_crouching) != m_prevStand && (!m_prone&&!m_crouching); }
	}
	
	public bool forwardKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKeyDown( KeyCode.W );
			else
				return Input.GetKeyDown( KeyCode.W ) && m_photonView.isMine; 
		}
	}
	
	public bool backKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKeyDown( KeyCode.S );
			else
				return Input.GetKeyDown( KeyCode.S ) && m_photonView.isMine; 
		}
	}
	
	public bool leftKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKeyDown( KeyCode.A );
			else
				return Input.GetKeyDown( KeyCode.A ) && m_photonView.isMine; 		
		}
	}
	
	public bool rightKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKeyDown( KeyCode.D );
			else
				return Input.GetKeyDown( KeyCode.D ) && m_photonView.isMine; 
		}
	}

	public bool forwardKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKey( KeyCode.W );
			else
				return Input.GetKey( KeyCode.W ) && m_photonView.isMine; 
		}
	}
	
	public bool backKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKey( KeyCode.S );
			else
				return Input.GetKey( KeyCode.S ) && m_photonView.isMine; 
		}
	}
	
	public bool leftKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKey( KeyCode.A );
			else
				return Input.GetKey( KeyCode.A ) && m_photonView.isMine; 		
		}
	}
	
	public bool rightKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKey( KeyCode.D );
			else
				return Input.GetKey( KeyCode.D ) && m_photonView.isMine; 
		}
	}
	
	public bool crouchKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetButtonDown("Crouch"); 
			else
				return Input.GetButtonDown("Crouch") && m_photonView.isMine; 
		}
	}
	
	public bool crouchKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetButton("Crouch");  
			else
				return Input.GetButton("Crouch") && m_photonView.isMine; 
			
		}
	}
	
	public bool jumpKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetKeyDown( KeyCode.Space ); 
			else
				return Input.GetKeyDown( KeyCode.Space ) && m_photonView.isMine; 
		}
	}
	
	public bool sprintKey{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetButton("Sprint"); 
			else
				return Input.GetButton("Sprint") && m_photonView.isMine; 
		}
	}

	public bool sprintKeyDown{
		get{ 
			if( PhotonNetwork.room == null )
				return Input.GetButtonDown("Sprint"); 
			else
				return Input.GetButtonDown("Sprint") && m_photonView.isMine; 
		}
	}
	
	public bool canFire{
		get{ 
			if( isDead ||  standing && sprintKey && forwardKey ||  ( prone && ( leftKey || rightKey || forwardKey || backKey ) ) || m_gunAnimController.actionsBlocked || carryingFlag )
				return false;
			else
				return true;
		}
	}
	
	public bool isAiming {
		get {
			return this.m_isAiming;
		}
		set {
			m_isAiming = value;
		}
	}
	
	public bool isDead{
		get{ return PlayerManager.GetIsDead( PhotonNetwork.player ); }
	}
	
	public PaintBall m_paintBall;
	public GrenadeEventController grenadeGO;
	public SmokeGrenadeEventController smokerGrenadeGO;

	private float dist = 0.0f;
	
	[HideInInspector]
	public Transform m_firePoint;
	public Transform m_pistolfirePoint;
	public Transform m_rifleFirePoint;
	public Transform m_grenadeFirePoint;
	
	public Vector3 firePosition;
	internal struct  State
	{
		internal double timestamp;
		internal Vector3 pos;
		internal Vector3 velocity;
		internal Quaternion rot;
		internal Vector3 angularVelocity;
	}
	
	// We store twenty states with "playback" information
	State[] m_BufferedState = new State[20];
	// Keep track of what slots are used
	int m_TimestampCount;
	
	private bool costumeAlreadySet;
	
	void Awake(){
		GameObject.DontDestroyOnLoad( gameObject );
		Respawn.RegisterPlayer( this );
	}
	
	void Start()
	{
		costumeAlreadySet = false;
		m_Controller = gameObject.GetComponent<SimpleScript>();
		m_weapons = GameObject.Find("FPSCamera").GetComponent<Weapon>();
		checkForJumpDamage = false;
		m_canShowGrenadeEffect = false;
		
		m_deadTime = (int)PhotonNetwork.time;
		m_photonView = PhotonView.Get( this );
		m_player = m_photonView.owner;
		
		if( m_photonView.isMine ){
			PlayerManager.SetIsDead( m_photonView.owner, true );
			
			NetMatch.instance.Initialize();
			
			//Nothing to wait on atm, lets say we're ready
			PlayerManager.SetIsReady( PhotonNetwork.player, true );
			    
			HUD.instance.skin = m_skin;
			
			NetPlayer.AddPlayer( this );
			
			this.GetComponent<AudioSource>().enabled = true;
		}
		
		m_footStepAudioPlayer = new FootStepAudio( m_footStepClips, gameObject );
		
		PlayerManager.SetPlayerColour(m_player, PlayerStatistics.instance.RGBToHex(PlayerStatistics.instance.paintballColor));
		
		PlayerManager.SetPlayerCostume(m_player, PlayerStatistics.instance.CostumeType);
		
		
		if(m_photonView.isMine)
		{
			SetFpsCostume();
			InvokeRepeating("TemporaryInvoke",0.0f,10.0f);
		}
	}
	
	
	void TemporaryInvoke()
	{
		m_photonView.RPC("SetTpsCostume",PhotonTargets.Others,PlayerStatistics.instance.CostumeType,PlayerStatistics.instance.HelmetType,PlayerStatistics.instance.MaskType);
	}
	
	[RPC]
	void SetTpsCostume(string costume,string helmet,string mask)
	{
		if(!costumeAlreadySet)
		{
			costumeAlreadySet = true;
			Debug.Log("Set TPS Costume        "+costume);
			
			if(tps_body)
				tps_body.renderer.material = tps_bodyMaterials[(int)GetMaterialNumber(costume)];
			
			if(tps_helmet)
				tps_helmet.renderer.material = tps_helmetMaterials[(int)GetMaterialNumber(helmet)];
			
			if(tps_mask)
				tps_mask.renderer.material = tps_maskMaterials[(int)GetMaterialNumber(mask)];	
		
			//m_photonView.RPC("CancelCostumeSet",m_photonView.owner);
		}
	}
	
	[RPC]
	void CancelCostumeSet()
	{
		Debug.Log("Costume already set set doont make a RPC call again");
		CancelInvoke("TemporaryInvoke");
	}
	
	void SetFpsCostume()
	{			
		Debug.Log("Set FPS Costume        "+PlayerStatistics.instance.CostumeType);
		
		if(fps_body)
			fps_body.renderer.material = fps_bodyMaterials[(int)GetMaterialNumber(PlayerStatistics.instance.CostumeType)];
				
	}
	
	int GetMaterialNumber(string name)
	{
		for(int i = 0 ; i < 4 ; i++)
		{
			if(name.Contains("0"))
				return 0;
			else if(name.Contains("1"))
				return 1;
			else if(name.Contains("2"))
				return 2;
			else if(name.Contains("3"))
				return 3;
			else
				return 0;
		}
		return 0;
	}
	
	void OnDestroy(){
		Respawn.UnRegisterPlayer( this );
	}
	
	void OnApplicationQuit(){
		Respawn.UnRegisterPlayer( this );	
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{		// Send data to server
		if (stream.isWriting)
		{
			NetTraffic.AddOutGoing();
			Vector3 pos = transform.position;
			Quaternion rot = transform.rotation;
			//Vector3 velocity = Vector3.zero;  //rigidbody.velocity;
			//Vector3 angularVelocity = Vector3.zero; // rigidbody.angularVelocity;

			stream.Serialize(ref pos);
			//stream.Serialize(ref velocity);
			stream.Serialize(ref rot);
			//stream.Serialize(ref angularVelocity);
		}
		// Read data from remote client
		else
		{
			NetTraffic.AddIncomming();
			
			Vector3 pos = Vector3.zero;
			Vector3 velocity = Vector3.zero;
			Quaternion rot = Quaternion.identity;
			Vector3 angularVelocity = Vector3.zero;
			stream.Serialize(ref pos);
			//stream.Serialize(ref velocity);
			stream.Serialize(ref rot);
			
			//transform.position = pos;
			//transform.rotation = rot;
			//return;
			//stream.Serialize(ref angularVelocity);
			
			// Shift the buffer sideways, deleting state 20
			for (int i=m_BufferedState.Length-1;i>=1;i--)
			{
				m_BufferedState[i] = m_BufferedState[i-1];
			}
			
			// Record current state in slot 0
			State state;
			state.timestamp = Time.time;
			state.pos = pos;
			state.velocity = velocity;
			state.rot = rot;
			state.angularVelocity = angularVelocity;
			m_BufferedState[0] = state;
			
			// Update used slot count, however never exceed the buffer size
			// Slots aren't actually freed so this just makes sure the buffer is
			// filled up and that uninitalized slots aren't used.
			m_TimestampCount = Mathf.Min(m_TimestampCount + 1, m_BufferedState.Length);

			// Check if states are in order, if it is inconsistent you could reshuffel or 
			// drop the out-of-order state. Nothing is done here
			for (int i=0;i<m_TimestampCount-1;i++)
			{
				if (m_BufferedState[i].timestamp < m_BufferedState[i+1].timestamp)
					Debug.Log("State inconsistent");
			}	
		}
		
	}
	
	//Messaging: Create a paintball;
	[RPC]
	private void CreateWepPaintball( Vector3 direction, Vector3 startPosition,Vector3 color,PhotonMessageInfo info){
		
		NetTraffic.AddIncomming();
		
		
		Color tempColor = new Color(color.x,color.y,color.z,1.0f);
		
		PaintBall ball = null;

		if( info.sender == PhotonNetwork.player ){
			ball = (PaintBall)GameObject.Instantiate( m_paintBall, m_firePoint.transform.position, Quaternion.identity);
			ball.ObjectColor = tempColor;
			ball.m_actualPos = Camera.main.transform.position;
			ball.m_virtualPos = m_firePoint.transform.position;
			ball.firePoint = m_firePoint;
		}
		else{
			ball = (PaintBall)GameObject.Instantiate( m_paintBall, startPosition, Quaternion.identity );
			ball.ObjectColor = tempColor;
			ball.m_actualPos = startPosition;
			ball.m_virtualPos = startPosition;
            Debug.Log("We are in the NetPlayer Class and we have fired a paintball this is for debug purposes");
		}
		
		ball.m_owner = info.sender;
		
		ball.m_velocity = direction * m_paintBall.m_initialVelocity;
	}
	
	[RPC]
	private void CreateSmokeGrenade( Vector3 forceSpeed, Vector3 startPosition, PhotonMessageInfo info)
	{
		NetTraffic.AddIncomming();
		SmokeGrenadeEventController smokeGrenade = null;
		
		if( info.sender == PhotonNetwork.player )
		{
			smokeGrenade = (SmokeGrenadeEventController)GameObject.Instantiate(smokerGrenadeGO, m_firePoint.transform.position, Quaternion.identity);
			smokeGrenade.m_actualPos = Camera.main.transform.position;
			smokeGrenade.m_virtualPos = m_firePoint.transform.position;
			smokeGrenade.firePoint = m_firePoint;
		}
		else
		{
			smokeGrenade = (SmokeGrenadeEventController)GameObject.Instantiate(smokerGrenadeGO, startPosition, Quaternion.identity);
            smokeGrenade.m_actualPos = startPosition;
			smokeGrenade.m_virtualPos = startPosition;
			Debug.Log("We are in the NetPlayer Class and we have fired a paintball this is for debug purposes");
		}
		
		smokeGrenade.ApplyForce(forceSpeed);
		smokeGrenade.ApplyTorque(100);
	}
	
	[RPC]
	private void CreateGrenade( Vector3 forceSpeed, Vector3 startPosition,Vector3 color, PhotonMessageInfo info)
	{
		NetTraffic.AddIncomming();
		GrenadeEventController grenade = null;
		
		Color tempColor = new Color(color.x,color.y,color.z,1.0f);
		
		if( info.sender == PhotonNetwork.player )
		{
			grenade = (GrenadeEventController)GameObject.Instantiate(grenadeGO, m_firePoint.transform.position, Quaternion.identity);
			grenade.ObjectColor = tempColor;
			grenade.m_actualPos = Camera.main.transform.position;
			grenade.m_virtualPos = m_firePoint.transform.position;
			grenade.firePoint = m_firePoint;
		}
		else
		{
			grenade = (GrenadeEventController)GameObject.Instantiate(grenadeGO, startPosition, Quaternion.identity);
			grenade.ObjectColor = tempColor;
            grenade.m_actualPos = startPosition;
			grenade.m_virtualPos = startPosition;
			Debug.Log("We are in the NetPlayer Class and we have fired a paintball this is for debug purposes");
		}
		
		grenade.ApplyForce(forceSpeed);
		grenade.ApplyTorque(100);
		
		grenade.m_owner = info.sender;
	}
	
	public AudioClip[] hitSounds = new AudioClip[0];
	
	[RPC]
	public void TookHit( float paintBallDamage, PhotonMessageInfo info )
	{
		NetTraffic.AddIncomming();
		m_hp -= paintBallDamage;// deduct paint ball damage
		Debug.Log( info.sender.name );// who got hit or who shot
		
		if(!audio.isPlaying)
			PlaySound(hitSounds[0]);
			//audio.PlayOneShot(hitSounds[0]);
		
		if( m_hp <= 0.0f && !m_dead ){
			m_dead = true;
			//We died, tell the network
			//if(!audio.isPlaying)
			//PhotonView photonView = PhotonView.Get( this );
			
			FPSCoordinator fpscoordRef = GameObject.Find("FPSCoordinator").GetComponent<FPSCoordinator>();
			fpscoordRef.PlayDeadAudio(info.sender);
			
			
			NetMatch.instance.PlayerKilled( PhotonNetwork.player, info.sender );
			//PlayerDead( PhotonNetwork.player, info.sender );
			Died();
		}
		//when somebody hit player, sound hit is loaded
		//if(!audio.isPlaying)
		//	audio.PlayOneShot(hitSounds[Random.Range(0,hitSounds.Length)]);
	}
	
//	void PlayerDead( PhotonPlayer dead, PhotonPlayer killer)
//	{
//		if(killer != null)
//		{
//			// don't raise points if killed by hismself
//			if(killer != PhotonNetwork.player)
//			{
//				m_photonView.RPC("PlayerDeadAudio", PhotonTargets.Others);
//			}
//		}
//	}
	
	public void PlaySoundOfHitting(bool isHead)
	{
		if(!audio.isPlaying)
		{
			if(isHead)
				PlaySound(hitSounds[2]);
				//audio.PlayOneShot(hitSounds[2]);
			else
				PlaySound(hitSounds[1]);
				//audio.PlayOneShot(hitSounds[1]);
		}
	}
		
	[RPC]
	public void FadeOut(){
		SetPrimComponentsEnabled( false );
	}
	
	[RPC]
	public void FadeIn(){
		SetPrimComponentsEnabled( true );
	}
	
		//if( prevLeft != m_leanLeft )
		//	m_photonView.RPC( "SetLeaningRight", PhotonTargets.All );
		//if( prevLeft != m_leanLeft )
		//	m_photonView.RPC( "SetLeaningLeft", PhotonTargets.All );	
	
	[RPC]
	public void SetLeaningRight( bool lean ){
		m_leanRight = lean;
	}
	
	[RPC]
	public void SetLeaningLeft( bool lean ){
		m_leanLeft = lean;
	}
	
	//Animation only RPCs
	[RPC]
	public void StanceChangeStand(){
		m_prone = false;
		m_crouching = false;
	}
	[RPC]
	public void StanceChangeProne(){
		m_prone = true;
		m_crouching = false;
	}
	[RPC]
	public void StanceChangeCrouch(){
		m_crouching = true;
		m_prone = false;
	}
	
	[RPC]
	public void WeaponChange( int type,int prevType ){
		m_bodyAnimController.StartSwapWeaponAnimations( ( GunType )type,(GunType) prevType );
	}
	
	[RPC]
	public void ReloadWeapon( int type )
	{
		m_bodyAnimController.StartWeaponReloadAnimation( ( GunType ) type );	
	}
	
	[RPC]
	public void RemoveTubes(int amountOfTubes)
	{
		for(int i = 0; i< m_tubes.Length - amountOfTubes; i++)
		{
			m_tubes[i].GetComponent<MeshRenderer>().enabled = false;
		}
	}
	
	[RPC]
	public void AddTubes(int amountOfTubes)
	{
		for(int i = 0; i< m_tubes.Length - amountOfTubes; i++)
		{
			m_tubes[i].GetComponent<MeshRenderer>().enabled = true;
		}
	}
	
	[RPC]
	public void FireWeapon( int type){
		m_bodyAnimController.StartWeaponFireAnimation( (GunType) type );
	}
	
	
	[RPC]
	public void StartGrenadePrepareAnimation()
	{
		m_bodyAnimController.PlayGrenadePrepareAnimation();	
	}
	
	[RPC]
	public void StartGrenadeThrowAnimation()
	{
		m_bodyAnimController.PlayGrenadeThrowAnimation();
	}
	
	//CTF stuff
	[RPC]
	void PickUpFlag(Transform player, Transform flag)
	{
		flag.parent = player.transform.FindChild("FlagCarryPoint");
		flag.localPosition = Vector3.zero;
		Debug.Log("Someone picked up a flag! is it on their back?");
	}
	
	[RPC]
	void CapturedFlag(Transform flag, int teamID)
	{
		flag.parent = GameObject.FindGameObjectWithTag("Team" + teamID.ToString() + "FlagArea").transform;
		flag.localPosition = Vector3.zero;
		flag.localRotation = Quaternion.identity;
		Debug.Log("Team " + teamID.ToString() + " Captured the flag!");
	}
	
	[RPC]
	void DropFlag(Transform flag)
	{
		flag.parent = null;
	}
	
	// We have a window of interpolationBackTime where we basically play 
	// By having interpolationBackTime the average ping, you will usually use interpolation.
	// And only if no more data arrives we will use extra polation
	void Update () 
	{	
		if(m_gunAnimController.currentWeapon == GunType.Rifle)
		{
			m_firePoint = m_rifleFirePoint;
		}
		else if(m_gunAnimController.currentWeapon == GunType.Pistol)
		{
			m_firePoint = m_pistolfirePoint;
		}
		else
		{
			m_firePoint = m_grenadeFirePoint;
		}
		
		m_trafficCalculated = false;
		
		HandleInterpolation();
		HandleLife();
		HandleVisability();
		
		if((m_Controller.isGameStarted)&&(!m_Controller.isGamePaused) && (!m_Controller.isChatActive))
		{
			Updateleaning();
			UpdateStance();
		}
		UpdateSounds();
		UpdateWeaponUsage();
		if(m_dead == false || standing == true || crouching == true)
		{
			m_footStepAudioPlayer.Update();
		}
		
		if(checkForJumpDamage)
		{
			FallDamage();
			// check if it is a jump or not 
			if (Physics.Raycast (transform.position, -Vector3.up,out hit)) 
			{
				if(hit.distance > 5.0f)
					grounded = false;
				else
					grounded = true;
			}
		}
		
		// to disable to starting spawn jump effect
		if(Controller.isGrounded)
			checkForJumpDamage = true;
	}
		
	public int GrenadeCount
	{
		set
		{
			grenadeCount = value;
		}
		get
		{
			return grenadeCount;
		}
	}
	
	public int SmokeGrenadeCount
	{
		set
		{
			smokeGrenadeCount = value;
		}
		get
		{
			return smokeGrenadeCount;
		}
	}
	
	private void FallDamage()
	{
		if(!grounded)
		{
			jumpHeight++; 
		}
		
		if(grounded && jumpHeight > jumpThreshold)
		{
			if(jumpHeight > 12)
				jumpHeight = 12;
			int damage = (jumpHeight - jumpThreshold)* 5;
			// to keep a check on damage
			Debug.Log("Do Damage here   "+jumpHeight+"      "+damage);
			m_hp -= damage;		// deduct fall damage
			//PlaySound(highFallSound[Random.Range(0,highFallSound.Length)]);
			audio.PlayOneShot(highFallSound[Random.Range(0,highFallSound.Length)]);
			if( m_hp <= 0.0f && !m_dead )
			{
				//We died, tell the network
				NetMatch.instance.PlayerKilled( PhotonNetwork.player, null);
				Died();
			}
			jumpHeight = 0;
		}
		else if(grounded)
		{
			jumpHeight = 0;
		}
				
		//if(jumpHeight > 3.0f)
		//	Debug.Log(jumpHeight);
	}
	
	void DeadAnimation()
	{
		Debug.Log("Play Dead Animation");
		SkinnedMeshRenderer[] renderers = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
		if( renderers != null )
		{
			for( int i = 0; i < renderers.Length; i++ )
			{
				//Destroy(renderers[i] );
				renderers[i].enabled = false;	
				//renderers[i].material.color = new Color(1,1,1,0.2f);
			}
		}
		
		Renderer[] meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();
		if(meshRenderers != null)
		{
			for( int i = 0; i < renderers.Length; i++ )
			{
				//Destroy(renderers[i] );	
				meshRenderers[i].enabled = false;
				//meshRenderers[i].material.color = new Color(1f,1f,1f,0.2f);
			}
		}
	}
	
	void OnTriggerEnter(Collider hit)
	{
		if(hit.gameObject.tag == "Death")
		{
			DoRespawn();
			//PlaySound(hitSounds[Random.Range(0,hitSounds.Length)]);
			audio.PlayOneShot(hitSounds[Random.Range(0,hitSounds.Length)]);
			Died();
		}
	}
	// Redo Input controlls.
   // bool currentWeaponStop = false;
	private void UpdateWeaponUsage(){
		
		if( m_photonView.isMine )
		{
			GunType prevGunType = m_gunAnimController.currentWeapon;
			
			if( m_gunAnimController.startedReload ){
				m_photonView.RPC( "ReloadWeapon", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon );
				m_photonView.RPC( "RemoveTubes", PhotonTargets.Others, 1);
			}

            else if (Input.GetKeyDown(KeyCode.Alpha1) && !m_gunAnimController.actionsBlocked)
            {
                m_gunAnimController.Switch(GunType.Rifle);
                m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }

            else if (Input.GetKeyDown(KeyCode.Alpha2) && !m_gunAnimController.actionsBlocked)
            {
				m_gunAnimController.Switch(GunType.Pistol);
                m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }
			else if (Input.GetKeyDown(KeyCode.Alpha3) && !m_gunAnimController.actionsBlocked)
            {
             	m_gunAnimController.Switch(GunType.Grenade);
               	m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }
			else if (Input.GetKeyDown(KeyCode.Alpha4) && !m_gunAnimController.actionsBlocked)
            {
             	m_gunAnimController.Switch(GunType.SmokeGrenade);
               	m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }

            if (Input.GetAxisRaw("Mouse ScrollWheel") > 0 && !m_gunAnimController.actionsBlocked)
            {
                m_gunAnimController.Switch(true);
                m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }
            else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0 && !m_gunAnimController.actionsBlocked)
            {
                m_gunAnimController.Switch(false);
                m_photonView.RPC("WeaponChange", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon,prevGunType);
            }
			
			if( m_gunAnimController.m_firing){
                FireWeapon((int)m_gunAnimController.currentWeapon);
				m_photonView.RPC( "FireWeapon", PhotonTargets.Others, (int)m_gunAnimController.currentWeapon );
			}
		}
		else{
			
			m_prevGunType = m_gunAnimController.currentWeapon;
			
		}
	}
	
	private void Updateleaning()
	{
		bool prevLeft = m_leanLeft;
		bool prevRight = m_leanRight;
		

		if( m_photonView.isMine || PhotonNetwork.room == null )
		{
			m_leanLeft = Input.GetButton("Leanleft") && !leanBlockedLeft;
			m_leanRight = Input.GetButton("Leanright") && !leanBlockedRight;
		}
		
		if( prevLeft != m_leanLeft )
			m_photonView.RPC( "SetLeaningLeft", PhotonTargets.Others, m_leanLeft );
		if( prevRight != m_leanRight )
			m_photonView.RPC( "SetLeaningRight", PhotonTargets.Others, m_leanRight );
		

        if (m_leanRight)
        {
            m_leaning = true;
            float dif = -m_maxLeanAngle - m_lean;
            m_lean += (dif * m_leanRate) * Time.deltaTime;
        }
       
        if (m_leanLeft)
        {
            m_leaning = true;
            float dif = m_maxLeanAngle - m_lean;
            m_lean += (dif * m_leanRate) * Time.deltaTime;
        }
        else
        {
            m_leaning = false;
        }
		
		if( !m_leanRight && !m_leanLeft ){
			m_lean -= (m_lean*m_leanRate)*Time.deltaTime;
		}
		
	}
	
	/*void OnPhotonPlayerConnected( PhotonPlayer player ){
		m_photonView.RPC( "WeaponChange", player, (int)m_gunAnimController.currentWeapon, (int)m_gunAnimController.currentWeapon );
	}*/
	
	private void UpdateStance(){
		
		m_prevProne = m_prone;
		m_prevCrouch = m_crouching;
		m_prevStand = !m_prone && !m_crouching;

		if( m_photonView.isMine || PhotonNetwork.room == null )
		{
			
			if( !crouchKey ){
				//We just started hitting the key
				m_crouchKeyTime = 0f;
			}		
			
			m_isAiming = Input.GetButton("Fire") && !sprintKey;
			
			if( m_crouching){
				float dif = m_maxCrouch - m_stance;
				m_stance += (dif*m_leanRate) * Time.deltaTime;
	
				if( crouchKey ){
					//We just started hitting the key
					m_crouchKeyTime += Time.deltaTime;
					//Debug.Log( "Time accumulating" );
				}
				
				if( crouchKeyDown ){
					m_crouching = false;
				}
			    
				if( m_crouchKeyTime > .4f )
                {
				    m_crouching = false;
					m_prone = true;
				}
				
				if( jumpKeyDown || sprintKey ){
					m_crouching = false;
				}
				
			}
			else if( m_prone ){
				float dif = m_maxProne - m_stance;
				m_stance += (dif*m_leanRate) * Time.deltaTime;
				
				if( crouchKeyDown ){
					m_prone = false;
					m_crouching = true;
				}		
				
				if( jumpKeyDown ){
					m_crouching = true;
					m_prone = false;
				}
				
				if( sprintKey ){
					m_prone = false;
					m_crouching = false;
				}
			}
			else{
				if( crouchKeyDown ){
					m_crouching = true;
				}			
				
				bool prevCrouch = m_crouching;
				if( prevCrouch != m_crouching )
				m_photonView.RPC( "SetLeaningLeft", PhotonTargets.All, m_leanLeft );
					
			}
	
			if( !m_crouching && !m_prone ){
				m_stance -= (m_stance*m_stanceRate)*Time.deltaTime;
			}
		
		}else{	
			if( m_crouching ){
				float dif = m_maxCrouch - m_stance;
				m_stance += (dif*m_leanRate) * Time.deltaTime;
			}
			else if( m_prone ){
				float dif = m_maxProne - m_stance;
				m_stance += (dif*m_leanRate) * Time.deltaTime;
			}

			if( !m_crouching && !m_prone ){
				m_stance -= (m_stance*m_stanceRate)*Time.deltaTime;
			}			
		}
		
		
		if( startedProne ){
		    m_photonView.RPC( "StanceChangeProne", PhotonTargets.Others );	
		}
		if( startedCrouch ){
			m_photonView.RPC( "StanceChangeCrouch", PhotonTargets.Others );	
		}
		if( startedStanding ){
			m_photonView.RPC( "StanceChangeStand", PhotonTargets.Others );	
		}
		
	}
	
	private void UpdateSounds(){
		if( !PlayerManager.GetIsDead( PhotonNetwork.player ) ){

			if( forwardKey || rightKey || leftKey || backKey ){
				m_footStepAudioPlayer.moving = true;
				
				if( standing ){
					if( sprintKey ){
						if( !rightKey && !leftKey && !backKey ){
							m_footStepAudioPlayer.currentSpeed = FootStepSpeed.Sprint;
						}
						else{
							m_footStepAudioPlayer.currentSpeed = FootStepSpeed.Run;
						}
					}
					else{
						m_footStepAudioPlayer.currentSpeed = FootStepSpeed.Run;
					}
					
					if( isAiming ){
						m_footStepAudioPlayer.currentSpeed = FootStepSpeed.Walk;
					}
					
				}
				
				if( crouching ){

					if( isAiming ){
						m_footStepAudioPlayer.currentSpeed = FootStepSpeed.CrouchAim;
					}
					else{
						m_footStepAudioPlayer.currentSpeed = FootStepSpeed.Walk;
					}
				}
				
				if( prone ){
					m_footStepAudioPlayer.moving = false;
				}
			}
			else{
				m_footStepAudioPlayer.moving = false;
			}
	
		
		}
		
	}
	
	private void HandleInterpolation(){
		//This is the target playback time of the rigid body
		double interpolationTime = Time.time - m_InterpolationBackTime;
		//Debug.Log( "Server time = " + Time.time );
		// Use interpolation if the target playback time is present in the buffer
		if (m_BufferedState[0].timestamp > interpolationTime)
		{
			// Go through buffer and find correct state to play back
			for (int i=0;i<m_TimestampCount;i++)
			{
				if (m_BufferedState[i].timestamp <= interpolationTime || i == m_TimestampCount-1)
				{
					// The state one slot newer (<100ms) than the best playback state
					State rhs = m_BufferedState[Mathf.Max(i-1, 0)];
					// The best playback state (closest to 100 ms old (default time))
					State lhs = m_BufferedState[i];
					
					// Use the time between the two slots to determine if interpolation is necessary
					double length = rhs.timestamp - lhs.timestamp;
					float t = 0.0F;
					// As the time difference gets closer to 100 ms t gets closer to 1 in 
					// which case rhs is only used
					// Example:
					// Time is 10.000, so sampleTime is 9.900 
					// lhs.time is 9.910 rhs.time is 9.980 length is 0.070
					// t is 9.900 - 9.910 / 0.070 = 0.14. So it uses 14% of rhs, 86% of lhs
					if (length > 0.0001){
						t = (float)((interpolationTime - lhs.timestamp) / length);
					}
					//	Debug.Log(t);
					// if t=0 => lhs is used directly
					transform.localPosition = Vector3.Lerp(lhs.pos, rhs.pos, t);
					transform.localRotation = Quaternion.Slerp(lhs.rot, rhs.rot, t);
					//Debug.Log( "Interpolating" );F
					return;
				}
			}
		}
		// Use extrapolation
		else
		{
			State latest = m_BufferedState[0];
			//PhotonNetwork.time
			float extrapolationLength = (float)(interpolationTime - latest.timestamp);
			// Don't extrapolation for more than 500 ms, you would need to do that carefully
			if (extrapolationLength < m_ExtrapolationLimit)
			{
				float axisLength = extrapolationLength * latest.angularVelocity.magnitude * Mathf.Rad2Deg;
				Quaternion angularRotation = Quaternion.AngleAxis(axisLength, latest.angularVelocity);
				
				transform.position = latest.pos + latest.velocity * extrapolationLength;
				transform.rotation = angularRotation * latest.rot;
				//rigidbody.velocity = latest.velocity;
				//rigidbody.angularVelocity = latest.angularVelocity;
			}
		}		
	}
	
	private void HandleLife(){

		if( m_photonView.isMine )
		{
			if( m_firstSpawn && m_dead )
			{
				//Debug.Log( "First spawn" );
				if( (((int)PhotonNetwork.time) - m_deadTime) > 10.0f )
				{
					DoRespawn();
					m_firstSpawn = false;
					m_showNextFrame = true;
				}
				
				return;
			}
			
			if( m_showNextFrame )
			{
				m_showNextFrame = false;
				m_dead = false;
				PlayerManager.SetIsDead( m_photonView.owner, false );
			}
			
			if( m_dead )
			{
				HUD.instance.life = (int)this.m_hp;
				HUD.instance.display = false;
				if( (((int)PhotonNetwork.time) - m_deadTime) > RoomManager.respawnWait )
				{
					DoRespawn();
					m_showNextFrame = true;
				}
			}
			else
			{	
				HUD.instance.display = true;
				HUD.instance.life = (int)this.m_hp;
			}
		}
	}
	
	private void HandleVisability()
	{
		SetPrimComponentsEnabled( !PlayerManager.GetIsDead( m_photonView.owner ) );	
	}
	
	public void PickedUpFlag()
	{
		if(m_photonView.isMine)
		{
			//attach flag to player
			Transform flag = GameObject.FindGameObjectWithTag("Team" + PlayerManager.GetTeam(player).ToString() + "Flag").transform;
			m_photonView.RPC("PickUpFlag",PhotonTargets.All, this.transform, flag);
		}
	}
	
	public void DroppedFlag()
	{
		Transform flag = GameObject.FindGameObjectWithTag("Team" + PlayerManager.GetTeam(player).ToString() + "Flag").transform;
		m_photonView.RPC("DropFlag", PhotonTargets.All, flag);
	}
	
	//We need to set ourself up to respawn, first of all that means it's time to fade out.
	private void Died()
	{	
		//DeadAnimation();
		
//		for( int i = 0; i < m_weapons.Length; i++ ){
//
//			m_weapons[i].ResetAmmo();
//
//		}
		
		Debug.Log( "I died" );
		m_dead = true;
		m_crouching = false;
		m_prone = false;
		PlayerManager.SetIsDead( m_photonView.owner, true );
		
		HUD.instance.display = false;
		
		//m_photonView.RPC( "FadeOut", PhotonTargets.AllBuffered );
	}
	
	private void DoRespawn(){
		//m_photonView.RPC( "FadeIn", PhotonTargets.AllBuffered );
		
		Vector3 spawnPoint = Respawn.GetSafeRespawnPoint( this );
		transform.position = spawnPoint;
		checkForJumpDamage = false;
		m_hp = 100;
		
		m_gunAnimController.PrepareForSpawn();
		m_gunAnimController.StartSpawnAnimation();
		
		Debug.Log("Reset player count");
		grenadeCount = 3;
		smokeGrenadeCount = 3;
		
		HUD.instance.display = true;
	}
	
	private void SetPrimComponentsEnabled( bool shouldEnable ){
		if( m_isVisible == shouldEnable )
			return;
		
		m_isVisible = shouldEnable;
		
		Debug.Log("In reset all things");
		
		//we will just dissapear for now
		Transform root = transform.root;
		
		//Grab all renderers here and disable them
		Renderer[] renderers = root.GetComponentsInChildren< Renderer >();
		for( int i = 0; i < renderers.Length; i++ )
		{
			renderers[i].enabled = shouldEnable;	
		}
		
		SkinnedDecalExpeditor[] decals = root.GetComponentsInChildren<SkinnedDecalExpeditor>();
		foreach(SkinnedDecalExpeditor myDecals in decals)
		{
			Debug.Log(myDecals.gameObject.name);
			Destroy(myDecals.gameObject);
		}
		
		DecalHolder[] decalHolders = root.GetComponentsInChildren<DecalHolder>();
		foreach(DecalHolder myDecals in decalHolders)
		{
			Debug.Log(myDecals.gameObject.name);
			Destroy(myDecals);
		}
		
		//Grab all colliders here and disable them
		Collider[] colliders = root.GetComponentsInChildren< Collider >();
		for( int i = 0; i < colliders.Length; i++ ){
			colliders[i].enabled = shouldEnable;	
		}
		
		CharacterController  charControl = root.GetComponentInChildren<CharacterController>();
		if( charControl != null )
			charControl.enabled = shouldEnable;
		
		FPSController fpsC = root.GetComponentInChildren<FPSController>();
		if( fpsC != null )
			fpsC.enabled = shouldEnable;
	}

	private void SetCollidersEnable( bool shouldEnable ){
		//we will just dissapear for now
		Transform root = transform.root;
		
		//Grab all colliders here and disable them
		Collider[] colliders = root.GetComponentsInChildren< Collider >();
		for( int i = 0; i < colliders.Length; i++ ){
			colliders[i].enabled = shouldEnable;	
		}

	}
	
	public void PlayCountdown()
	{
		if(!audio.isPlaying)
		{
			audio.clip = m_countdownClip;
			audio.Play();
		}
		else
			return;
	}
	
	void PlaySound(AudioClip sound)
	{
		audio.clip = sound;
		//audio.volume = vol;
		audio.Play();
	}
}