using UnityEngine;
using System.Collections;
#pragma warning disable 0414
public static class NetTraffic{
	
	private static int m_outCount, m_inCount, m_lastIn, m_lastOut;
	private static float	m_lastDumpTime, m_time;
	
	static NetTraffic(){
		m_lastDumpTime = Time.time;	
	}
	
	public static void AddOutGoing(){
		m_outCount += 1;
	}
	
	public static void AddIncomming(){
		m_inCount += 1;
	}
	
	public static int DumpCountToGUI( Rect rect ){
		
		m_time += Time.deltaTime;
		
		if( m_time > 1.0f ){
			m_time = 0.0f;
			m_lastIn = m_inCount;
			m_lastOut = m_outCount;
			m_outCount = 0;
			m_inCount = 0;
		}
	
		//GUI.Label( rect, "Outgoing Messages in last second: " + m_lastOut.ToString() );
		//rect.y += 30;
		//GUI.Label( rect, "Incomming Messages in last second: " + m_lastIn.ToString() );
		

		
		m_lastDumpTime = Time.time;
		return m_outCount;
	}
	
	public static bool IsTimeToDump(){
			return true;
	}
	
}

