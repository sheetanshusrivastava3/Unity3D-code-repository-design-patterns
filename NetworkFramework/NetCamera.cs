using UnityEngine;
using System.Collections;

public class NetCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if( !PhotonView.Get( this ).isMine ){
			Destroy( gameObject );
		}
	}
	
	// Update is called once per frame
	void Update () {
		if( !PhotonView.Get( this ).isMine ){
			camera.enabled = false;
			return;
		}	
	}
	
	void OnPhotonInstantiate( PhotonMessageInfo info ){
		
	}
	
	
	//PhotonNetworkingMessage.OnPhotonInstantiate
}
