using UnityEngine;
using System.Collections;

public class NetObjectKiller : MonoBehaviour {
	
	public bool m_ifIsMine;
	public bool m_gameObject, m_renderers;
	public Component[] m_components;
	
	public GameObject[] m_go;
	
	public PhotonView m_view;
	
	// Use this for initialization
	void Start () {
		if( !PhotonNetwork.connected )
			return;
		
		if( m_view == null ){
			Debug.Log( gameObject.name );// tell me what this is connected to 
		}
		
		Debug.Log(m_view.isMine);
		Debug.Log(m_ifIsMine);
		if( !m_view.isMine && !m_ifIsMine ){
			if( m_gameObject ){
				Destroy( gameObject );
				return;
			}	
			
			MeshRenderer[] renderer = gameObject.GetComponentsInChildren<MeshRenderer>();
			if( renderer != null ){
				for( int i = 0; i < renderer.Length; i++ ){
					Destroy( renderer[i] );
				}
			}
			
			if( m_renderers ){
				SkinnedMeshRenderer[] renderers = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
				if( renderers != null ){
					for( int i = 0; i < renderers.Length; i++ ){
						Debug.Log ("HELLOOOOOOO" + renderers[i].gameObject.name);
						Destroy( renderers[i] );
					}
				}
				
//				MeshRenderer[] renderer = gameObject.GetComponentsInChildren<MeshRenderer>();
//				if( renderer != null ){
//					for( int i = 0; i < renderer.Length; i++ ){
//						Destroy( renderer[i] );
//					}
//				}
			}
			
			for( int i = 0; i < m_components.Length; i++ ){
				Destroy( m_components[i] );	
			}
			
			for( int i = 0; i < m_go.Length; i++ ){
				Destroy( m_go[i] );	
			}
			
			Destroy( this );
			return;
		}
		
		if( m_view.isMine && m_ifIsMine ){
			if( m_gameObject ){
				Destroy( gameObject );
				return;
			}	
			
			MeshRenderer[] renderer = gameObject.GetComponentsInChildren<MeshRenderer>();
			if( renderer != null ){
				for( int i = 0; i < renderer.Length; i++ ){
					Destroy( renderer[i] );
				}
			}
			
			if( m_renderers ){
				SkinnedMeshRenderer[] renderers = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
				if( renderers != null ){
					for( int i = 0; i < renderers.Length; i++ ){
						Debug.Log ("HELLOOOOOOO" + renderers[i].gameObject.name);
						Destroy( renderers[i] );	
					}
				}
//				MeshRenderer[] renderer = gameObject.GetComponentsInChildren<MeshRenderer>();
//				if( renderer != null ){
//					for( int i = 0; i < renderer.Length; i++ ){
//						Destroy( renderer[i] );
//					}
//				}
			}
			
			for( int i = 0; i < m_components.Length; i++ ){
				Destroy( m_components[i] );	
			}
			
			for( int i = 0; i < m_go.Length; i++ ){
				Destroy( m_go[i] );	
			}
			
			Destroy( this );
			return;
		}
	}
}
