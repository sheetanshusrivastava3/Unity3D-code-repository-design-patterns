using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class FPSTeam{
	
	private List< PhotonPlayer > m_players = new List<PhotonPlayer>();
	
	//Build the m_players list from the PhotonNetwork.playerList list
	public FPSTeam( int teamID )
	{
		for( int i = 0; i < PhotonNetwork.playerList.Length; i++ )
		{
			if( PlayerManager.GetTeam( PhotonNetwork.playerList[i] ) == teamID )
			{
				m_players.Add( PhotonNetwork.playerList[i] );
			}
		}
	}
	
	//Adds up the kills of all players and returns the sum
	public int FetchTeamKills(){
		
		int total = 0;
		for( int i = 0; i < m_players.Count; i++ ){
			total += PlayerManager.GetKillCount( m_players[i] );
		}
		
		return total;
		
	}
	
	//Adds up the goal score from each player and returns the sum
	public int FetchTeamGoalScore(){
		
		int total = 0;
		for( int i = 0; i < m_players.Count; i++ ){
			total += PlayerManager.GetGoalScore( m_players[i] );
		}
		
		return total;
		
	}
	
}


public static class TeamManager
{
	
	//Implemented later - This setting will cause the TeamManager to automatically reballance teams when a to be implemented
	//"ValidateTeams" method is available
	private static bool	m_autoBallance;
	
	//Is the match a team game?
	public static bool isTeamGame{
		get{ return IsMatchTeamBased(); }
	}
	
	//Get or set the minimum players per team
	public static int minPlayersPerTeam {
		get {
			return GetMinPlayersPerTeam();
		}
		set {
			SetMinPlayersPerTeam( value );
		}
	}
	
	//Set or get the number of teams in the room
	public static int teamCount {
		get {
			return GetTeamCount();
		}
		set {
			SetTeamCount( value );
		}
	}
	
	private static bool IsMatchTeamBased(){
		
		if( RoomManager.gameType == NetGameType.Unresolved ){
			Debug.LogError( "Game type is not set or was set invalidly, can't determine team play status" );
			return false;
		}
		
		switch( RoomManager.gameType ){
		case NetGameType.FreeForAll:
			return false;
			
		case NetGameType.TeamMatch:
			return true;
			
		case NetGameType.CaptureTheFlag:
			return true;
		}
		
		return false;
		
	}
	
	public static int GetMinPlayersPerTeam(){
		int minPlayers = 0;
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return 99999;
		}
		
		if( PropertyHandler.GetRoomInt( "MinPlayersPerTeam", ref minPlayers ) ){		
			return minPlayers;
		}
		else{
			Debug.LogError( "MinPlayersPerTeam property not set in room, make sure it is set first" );
			return 99999;
		}
	}

	public static void SetMinPlayersPerTeam( int count ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "MinPlayersPerTeam", count);
		
		PhotonNetwork.room.SetCustomProperties( table );

	}
	
	public static int GetTeamCount(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return 99999;
		}
		
		int teamCount = 0;
		if( PropertyHandler.GetRoomInt( "TeamCount", ref teamCount ) ){		
			return teamCount;
		}
		else{
			Debug.LogError( "TeamCount property not set in room, make sure it is set first" );
			return 99999;
		}
	}

	public static void SetTeamCount( int count ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "TeamCount", count);
		
		PhotonNetwork.room.SetCustomProperties( table );

	}
	
	
	public static bool AreEnoughPlayersForTeams(){

		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return false;
		}

		if( PhotonNetwork.room.playerCount >= minPlayersPerTeam * teamCount )
			return true;
		else
			return false;
	}
	
	//Sort players into teams using a simple "sorted by rank" mechnism later.
	//For now just go through the player list and stick each one on one side or the other
	public static void SetPlayerTeams(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
		
		if( !AreEnoughPlayersForTeams() ){
			Debug.LogError( "Not enough players present to create the desired number of teams" );
			return;
		}
		
		int curTeam = 0;
		for( int i = 0; i < PhotonNetwork.room.playerCount; i++ ){
			PlayerManager.SetTeam( PhotonNetwork.playerList[i], curTeam );
			curTeam++;
			
			if( curTeam > teamCount-1 )
				curTeam = 0;
		}
		
	}
	
}

