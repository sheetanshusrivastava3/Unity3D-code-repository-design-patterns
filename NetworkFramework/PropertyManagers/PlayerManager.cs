using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class FPSPlayer{
	
}

//Customization, name, sync to DB, etc.
public static class PlayerManager
{
	public static void SetCarryingFlag(PhotonPlayer player, bool isCarrying)
	{
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "FlagCarrier", isCarrying );
		
		player.SetCustomProperties( table );
	}
	
	public static void SetTeam( PhotonPlayer player, int teamID ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "TeamID", teamID );
		
		player.SetCustomProperties( table );
	}
	
	public static int GetTeam( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
		
		int teamID = -1;
		if( PropertyHandler.GetPlayerInt( player, "TeamID", ref teamID ) )
			return teamID;
			
		return -1;
	}
	
	public static void SetPlayerCostumeType(PhotonPlayer player,string type)
	{
		
	}
	
	public static void SetKillCount( PhotonPlayer player, int kills ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "KillCount", kills );
		
		player.SetCustomProperties( table );
	}
		
	public static int GetKillCount( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
		
		int killCount = -1;
		if( PropertyHandler.GetPlayerInt( player, "KillCount", ref killCount ) )
			return killCount;
			
		return -1;
	}
	
	
	public static void SetShotsFired(PhotonPlayer player,int fireCount)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return;
		
		Hashtable table = new Hashtable();
		table.Add("ShotsFired",fireCount);
		
		player.SetCustomProperties(table);
	}
	
	public static int GetShotsFired( PhotonPlayer player )
	{	
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
			
		int shotsFired = -1;
		if( PropertyHandler.GetPlayerInt( player, "ShotsFired", ref shotsFired ) )
			return shotsFired;
				
		return -1;
	}
	
	public static void SetHeadshots(PhotonPlayer player,int headSHots)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return;
		
		Hashtable table = new Hashtable();
		table.Add("HeadShots",headSHots);
		
		player.SetCustomProperties(table);
	}
	
	public static int GetHeadshots( PhotonPlayer player )
	{	
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
			
		int headShots = -1;
		if( PropertyHandler.GetPlayerInt( player, "HeadShots", ref headShots ) )
			return headShots;
				
		return -1;
	}
	
	
	public static void SetDeathCount( PhotonPlayer player, int deaths ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "DeathCount", deaths );
		
		player.SetCustomProperties( table );
	}
	
	public static int GetDeathCount( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
		
		int deathCount = -1;
		if( PropertyHandler.GetPlayerInt( player, "DeathCount", ref deathCount ) )
			return deathCount;
			
		return -1;
	}
	
	public static string GetKillDeathRatio(PhotonPlayer player)
	{
		var gcd = GCD(GetKillCount(player), GetDeathCount(player));
		int a = GetKillCount(player);
		int b = GetDeathCount(player);
		if(a == 0 || b == 0)
			return "0:0";
		else
			return string.Format("{0}:{1}", a / gcd, b / gcd);
	}
	
	internal static int GCD(int a, int b) 
	{
		return b == 0 ? a : GCD(b, a % b);
	}
	
	public static void SetGoalScore( PhotonPlayer player, int goalScore ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "GoalScore", goalScore );
		
		player.SetCustomProperties( table );
	}
	
	public static int GetGoalScore( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return -1;
		
		int goalScore = -1;
		if( PropertyHandler.GetPlayerInt( player, "GoalScore", ref goalScore ) )
			return goalScore;
			
		return -1;
	}
	
	public static void SetPlayerPoints(PhotonPlayer player, int playerPoints)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return;
		
		Hashtable table = new Hashtable();
		table.Add("PlayerPoints", playerPoints);
		
		player.SetCustomProperties(table);
	}
	
	public static int GetPlayerPoints(PhotonPlayer player)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return -1;
		
		int playerPoints = -1;
		
		if(PropertyHandler.GetPlayerInt(player, "PlayerPoints", ref playerPoints))
			return playerPoints;
		
		return -1;
	}
	
	public static void SetPlayerColour(PhotonPlayer player, string colorHex)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return;
		Hashtable table = new Hashtable();
		table.Add("PaintballColour", colorHex);
		
		player.SetCustomProperties(table);
	}
	
	public static Color GetPlayerColour(PhotonPlayer player)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return new Color(235.0f/255.0f,146.0f/255.0f,14.0f/255.0f,1.0f);
		string hex = (string)player.customProperties["PaintballColour"];
		Debug.Log("Color --------  "+hex);
		return PlayerStatistics.instance.HexToRGB(hex);
	}
	
	public static string GetPLayerCostume(PhotonPlayer player)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return "Costume1";
		return (string)player.customProperties["CostumeType"];
	}
	
	public static void SetPlayerCostume(PhotonPlayer player,string type)
	{
		if(!PhotonHelper.CanInteractWithProperties())
			return;
		Hashtable table = new Hashtable();
		table.Add("CostumeType", type);
		
		player.SetCustomProperties(table);
	}
	
	public static void SetIsDead( PhotonPlayer player, bool dead ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "IsDead", dead );
		
		player.SetCustomProperties( table );
	}
	
	public static bool GetIsDead( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return false;
		
		bool isDead = false;
		if( PropertyHandler.GetPlayerBool( player, "IsDead", ref isDead ) )
			return isDead;
			
		return false;
	}	
	
	public static void SetIsReady( PhotonPlayer player, bool ready ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "IsReady", ready );
		
		player.SetCustomProperties( table );
	}
	
	public static bool GetIsReady( PhotonPlayer player ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return false;
		
		bool isReady = false;
		if( PropertyHandler.GetPlayerBool( player, "IsReady", ref isReady ) )
			return isReady;
			
		return false;
	}	
	
	public static void ClearPlayerIsReadyFlags( ){
		if( !PhotonHelper.CanInteractWithProperties( ) )
			return;
		
		Hashtable table = new Hashtable();
		table.Add( "IsReady", false );
		
		for( int i = 0; i<PhotonNetwork.playerList.Length; i++ ){
			PhotonNetwork.playerList[i].SetCustomProperties( table );
		}
		
	}
	

	
	//Set starting values on all match stats
	public static void PreparePlayersForMatch(){//DM only thus far
		//Clear all player scores/deaths to 0
		for( int i = 0; i < PhotonNetwork.room.playerCount; i++ ){
			PreparePlayer( PhotonNetwork.playerList[i] );
		}
	}
	
	//Prepare a player for the current match
	public static void PreparePlayer( PhotonPlayer player )
	{
		SetKillCount( player, 0);
		SetDeathCount( player, 0);
		SetGoalScore( player, 0);
		SetPlayerPoints(player, 0);
		SetIsDead( player, true);
		SetShotsFired(player,0);
		SetHeadshots(player,0);
	}
	
	
}
