using UnityEngine;
using System.Collections;

public static class PhotonHelper
{
	
	//Can we interact with room attributes?
	public static bool CanInteractWithProperties(){
		if( !PhotonNetwork.connected ){//Not connected, obviously no way to mod attributes
			Debug.LogWarning( "Not connected, can't interact with room attributes" );
			return false;
		}

		if( PhotonNetwork.room == null ){//We aren't in a room
			Debug.LogWarning( "Not in a room, can't interact with room attributes" );
			return false;
		}
		else
		return true;		
	}
	
}

