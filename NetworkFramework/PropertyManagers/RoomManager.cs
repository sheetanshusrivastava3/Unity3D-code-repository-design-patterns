using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

//Unresolved game type means the room had something wrong with it's game type setting
//The other types are self explanatory
public enum NetGameType{ FreeForAll, TeamMatch, CaptureTheFlag, Unresolved };
public enum NetRoomState{ InReadyRoom, InMatch, Unresolved };
public enum NetRespawnUsage{ Teams, Open, Unresolved };

public static class RoomManager {
	
	private static string		m_roomLevel;//What level is the room running currently?(Or planning to run)
	private static float		m_gameTime;
	
	//What type of game are we currently setting properties for?
	public static NetGameType gameType {
		get{
			return RoomManager.GetRoomGameType();
		}
		set{
			RoomManager.SetRoomGameType( value );
		}
	}
	
	//What state is the room in? Match, ready room etc.
	public static NetRoomState state {
		get{
			return GetRoomState();
		}
		set{
			SetRoomState( value );
		}
	}
	
	//What state is the room in? Match, ready room etc.
	public static int minPlayerCount {
		get{
			return GetRoomMinPlayers();
		}
		set{
			SetRoomMinPlayers( value );
		}
	}
	
	//What state is the room in? Match, ready room etc.
	public static int scoreLimit {
		get{
			return GetRoomScoreLimit();
		}
		set{
			SetRoomScoreLimit( value );
		}
	}
	
	//The respawn wait for the room.
	public static int respawnWait {
		get{
			return GetRoomRespawnWait();
		}
		set{
			SetRoomRespawnWait( value );
		}
	}
	
	//The respawn usage for the upcomming match
	public static NetRespawnUsage respawnUsage{
		get{ return GetRoomRespawnUsage(); }
		set{ SetRoomRespawnUsage( value ); }
	}
	
	//Who won the last FFA match?
	public static string FFAWinnerName{
		get{ return GetFFAWinner(); }
		set{ SetFFAWinner( value ); }
	}
	
	//Are we waiting on players for something?
	public static bool waitingForPlayers{
		get{ return GetRoomWaitingForPlayers(); }
		set{ SetRoomWaitingForPlayers( value ); }
	}
	
	//Ask a room what game type it is running
	private static NetGameType GetRoomGameType( ){
		
		if( !PhotonHelper.CanInteractWithProperties( )){
			return NetGameType.Unresolved;
		}
	
		int gameType = 0;
		if( PropertyHandler.GetRoomInt( "GameType", ref gameType ) )
		{		
			return (NetGameType)gameType;
		}
		else
			return NetGameType.Unresolved;
		
	}
	
	//Set the game type for a room
	private static void SetRoomGameType( NetGameType type ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		if( type == NetGameType.Unresolved ){
			Debug.LogError( "The 'NetGameType.Unresolved' type is not a valid game type to set" );
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "GameType", (int)type);
		
		PhotonNetwork.room.SetCustomProperties( table );
		
	}
	
	//Ask a room what the minimum player count to launch a match is
	private static int GetRoomMinPlayers( ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return 9999;
		}
	
		int minPlayers = 0;
		if( PropertyHandler.GetRoomInt( "MinPlayerCount", ref minPlayers ) ){		
			return minPlayers;
		}
		else{
			return 9999;
		}
		
	}
	
	//Set the minimum number of players we need to launch a match
	private static void SetRoomMinPlayers( int minPlayerCount ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
		
		if( minPlayerCount < 1 ){
			Debug.LogError( "minPlayerCount must be 1 or greater, not setting player count" );
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "MinPlayerCount", minPlayerCount);
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static void SetRoomTimeLimit(float time)
	{
		if(!PhotonHelper.CanInteractWithProperties())
		{
			return;
		}
		
		Hashtable table = new Hashtable();
		table.Add("TimeLimit",time);
		
		PhotonNetwork.room.SetCustomProperties(table);
	}
	
	private static NetRoomState GetRoomState(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return NetRoomState.Unresolved;
		}				
		
		int state = -1;
		if( PropertyHandler.GetRoomInt( "GameState", ref state ) )
			return (NetRoomState)state;
		else
			return NetRoomState.Unresolved;
		
		
	}
	
	private static void SetRoomState( NetRoomState state ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}		
		
		if( state == NetRoomState.Unresolved ){
			Debug.LogError( "The 'NetRoomState.Unresolved' state is not a valid game type to set" );
			return;
		}
		
		Hashtable table = new Hashtable();
		table.Add( "GameState", (int)state );
		
		PhotonNetwork.room.SetCustomProperties( table );		
		
	}
		
	//Set the score limit for the match
	private static void SetRoomScoreLimit( int limit ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
		
		if( limit < 1 ){
			Debug.LogError( "Score Limit must be 1 or greater, not setting player count" );
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "ScoreLimit", limit);
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static int GetRoomScoreLimit(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return 9999;
		}				
		
		int limit = -1;
		if( PropertyHandler.GetRoomInt( "ScoreLimit", ref limit ) )
			return limit;
		else
			return 9999;

	}
	
	//Set the score limit for the match
	private static void SetRoomRespawnWait( int respawnWait ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
		
		if( respawnWait < 0 ){
			Debug.LogError( "Respawn wait must be 0 or greater, not setting respawn wait" );
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "RespawnWait", respawnWait);
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static int GetRoomRespawnWait(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return 9999;
		}				
		
		int respawnWait = -1;
		if( PropertyHandler.GetRoomInt( "RespawnWait", ref respawnWait ) )
			return respawnWait;
		else
			return 9999;

	}
	
	//Set the score limit for the match
	private static void SetFFAWinner( string playerName ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "FFAWinner", playerName );
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static string GetFFAWinner( ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return "";
		}				
		
		string name = "";
		if( PropertyHandler.GetRoomString( "FFAWinner", ref name ) )
			return name;
		else
			return "";

	}
	
	//Set the score limit for the match
	private static void SetRoomWaitingForPlayers( bool waiting ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "WaitingForPlayers", waiting );
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static bool GetRoomWaitingForPlayers( ){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return false;
		}				
		
		bool waiting = false;
		if( PropertyHandler.GetRoomBool( "WaitingForPlayers", ref waiting ) )
			return waiting;
		else
			return false;

	}
	
	//Set the score limit for the match
	private static void SetRoomRespawnUsage( NetRespawnUsage usage ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "RespawnUsage", (int)usage);
		
		PhotonNetwork.room.SetCustomProperties( table );
				
	}
	
	private static NetRespawnUsage GetRoomRespawnUsage(){
		
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return NetRespawnUsage.Unresolved;
		}				
		
		int usage = 0;
		if( PropertyHandler.GetRoomInt( "RespawnUsage", ref usage ) )
			return ( NetRespawnUsage )usage;
		else
			return NetRespawnUsage.Unresolved;

	}
	
	public static void SetRoomLevel( string levelName ){
		if( !PhotonHelper.CanInteractWithProperties( ) ){
			return;
		}
			
		Hashtable table = new Hashtable();
		table.Add( "LevelName", levelName );
		PhotonNetwork.room.SetCustomProperties( table );
	}
		                         
	public static string GetRoomLevel( RoomInfo room ){			
		
		string level = "";
		if( PropertyHandler.GetRoomInfoString( room,  "LevelName", ref level ) )
			return level;
		else
			return "";
		
	}
	
	public static NetRoomState GetRoomState( RoomInfo info ){		
		
		int state = -1;
		if( PropertyHandler.GetRoomInfoInt( info, "GameState", ref state ) )
			return (NetRoomState)state;
		else
			return NetRoomState.Unresolved;
		
	}
	
	public static float GetRoomTime()
	{
		Debug.Log("Getting the room time");
		if(PhotonNetwork.isMasterClient)
		{
			m_gameTime =  Time.timeSinceLevelLoad;
			Hashtable table = new Hashtable();
			table.Add("GameTime", (float)m_gameTime);
			//table["GameTime"] = (float)m_gameTime;
			PhotonNetwork.room.SetCustomProperties(table);
			Debug.Log("We are the host and we updated the time" + (string)PhotonNetwork.room.customProperties["GameTime"].ToString());
			
			return (float)PhotonNetwork.room.customProperties["TimeLimit"] - m_gameTime;
		}
		else if (!PhotonNetwork.isMasterClient)
		{
			Debug.Log("We are NOT the host and we grabbed the gametime" + (string)PhotonNetwork.room.customProperties["GameTime"].ToString());
			float time  =  (float)PhotonNetwork.room.customProperties["TimeLimit"] - (float)PhotonNetwork.room.customProperties["GameTime"];
			time -= 3.0f;
			return time;
			//return (float)PhotonNetwork.room.customProperties["TimeLimit"] - (float)PhotonNetwork.room.customProperties["GameTime"];
		}
		else
		{
			Debug.Log("We tried to set the time but something went wrong!");
			return -1;
		}
	}
}
