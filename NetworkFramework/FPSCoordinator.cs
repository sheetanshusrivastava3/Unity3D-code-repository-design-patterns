using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Hashtable = ExitGames.Client.Photon.Hashtable;

#pragma warning disable 0414
public class FPSNetRoom{
	
	private RoomInfo m_room;
	
	public FPSNetRoom( RoomInfo room ){
		m_room = room;
	}
	
	public string name{
		get{ return m_room.name; }	
	}

	public RoomInfo roomInfo{
		get{ return m_room; }	
	}
	
	public bool ToGUI( Rect rect, bool asLabel ){
		bool selected = false;
		
		float curX = 0f;
		//Room name on left, name will allow 40% of full width
		Rect nameRect = new Rect( rect.x, rect.y, rect.width*.4f, rect.height );
		if(!asLabel){
			if( GUI.Button( nameRect, new GUIContent( m_room.name ) ) ){
				selected = true;
			}
		}
		else{
			GUI.Label( nameRect, new GUIContent( m_room.name ) );
		}
		
		
		curX += rect.width*.4f;
		
		//playerCount next, allow 10% of width
		Rect playerCountRect = new Rect( curX, rect.y, rect.width*.1f, rect.height );
		if(!asLabel){
			if( GUI.Button( playerCountRect, new GUIContent( m_room.playerCount.ToString() + "/" + m_room.maxPlayers.ToString() ) ) )
				selected = true;
		}
		else
		{
			GUI.Label( playerCountRect, new GUIContent( m_room.playerCount.ToString() + "/" + m_room.maxPlayers.ToString() ) );
		}
		
		curX += playerCountRect.width;
		//LevelNotifyAboutChatName next, allow 50% of width
		Rect levelNameRect = new Rect( curX, rect.y, rect.width*.5f, rect.height );
		if(!asLabel){
			if( GUI.Button(levelNameRect, new GUIContent( (string)m_room.customProperties["LevelName"] ) ) )
				selected = true;
		}
		else{
			GUI.Label( playerCountRect, new GUIContent( (string)m_room.customProperties["LevelName"] ) );
		}
		
		return selected;
	}
	
	public bool ToGUI( Rect rect, bool asLabel, GUIStyle style ){
		bool selected = false;
		
		float curX = 0f;
		//Room name on left, name will allow 40% of full width
		Rect nameRect = new Rect( rect.x, rect.y, rect.width*.4f, rect.height );
		if(!asLabel){
			if( GUI.Button( nameRect, new GUIContent( m_room.name ), style ) ){
				selected = true;
			}
		}
		else{
			GUI.Label( nameRect, new GUIContent( m_room.name ), style );
		}
		
		curX += rect.width*.4f;
		
		//playerCount next, allow 10% of width
		Rect playerCountRect = new Rect( curX, rect.y, rect.width*.1f, rect.height  );
		if(!asLabel){
			if( GUI.Button( playerCountRect, new GUIContent( m_room.playerCount.ToString() + "/" + m_room.maxPlayers.ToString() ), style ) )
				selected = true;
		}
		else{
			GUI.Label( playerCountRect, new GUIContent( "Players" ), style );
		}
		
		return selected;
	}
	
	public void Connect(){
		PhotonNetwork.JoinRoom( m_room.name );
		//PhotonNetwork.JoinRoom( m_room );
	}
	
}

public class CreateRoomSettings
{
	
	public NetGameType 	m_gameType;
	public int 			m_respawnWait = 3;
	public int 			m_scoreLimit = 0;
	public string		m_roomName = "";
	public int			m_maxPlayers;
	public string		m_levelName;
	public float		m_timeLimit = 60;
    public ReadyRoom    readyRoom;
	public string[] bunkerNamesArray;
	public Vector3[] bunkerPositionsArray; 
	public Vector3[] bunkerRotationsArray; 
}

	//****************************
	//MasterClient Only stuff here
	//****************************
public class FPSCoordinator: MonoBehaviour{
	
	
	//START SINGLETON
    private static FPSCoordinator m_instance;

	private int dayVotes;
	private int sunsetVotes;
	private int nightVotes;
		
	[System.NonSerialized]	public string[] bunkerNamesArray;
	[System.NonSerialized]	public Vector3[] bunkerPositionsArray; 
	[System.NonSerialized]	public Vector3[] bunkerRotationsArray; 
	
	[SerializeField] private AudioClip endgameAudio;
	[SerializeField] private AudioClip playerJoinedAudio;
	[SerializeField] private AudioClip playerDeadBoomAudio;
	
	public static FPSCoordinator instance
    {
        get
        {
            if (m_instance == null)
            {
				m_instance = GameObject.Find( "FPSCoordinator" ).GetComponent<FPSCoordinator>();
				m_instance.m_photonView = m_instance.gameObject.GetComponent<PhotonView>();
            }

            return m_instance;
        }
    }

	[Serializable]
	public class SubScenes
	{
		public string headName;
		public string daySceneName;
		public string sunsetSceneName;
		public string nightSceneName;
	}
	
	public SubScenes[] subScenes;
	
	public static void Release(){
		
		if( m_instance != null ){
			Destroy( m_instance.gameObject );	
		}
	}
   
    public void OnApplicationQuit()
    {
        m_instance = null;
    }
	
	private string customMapName;
	
	public string CustomMapName
	{
		get
		{
			return customMapName;
		}
		set
		{
			customMapName = value;
		}
	}
	
	private string labelname = "";
	public string menuLabelName
	{
		get
		{
			return labelname;
		}
		set
		{
			labelname = value;
		}
	}
	
	//END SINGLETON
	
	/**************************
	 * STATICS
	 **************************/
	public string m_currentNetVersion = ".01";
	private int m_minPlayers = 1;//Minimum number of players to start the match
	private Vector2 m_sensitivity = new Vector2(5.0f, 5.0f);
	public Vector2 sensitivity
	{
		get{ return instance.m_sensitivity;}
		set{ instance.m_sensitivity = value;}
	}
	
	//Ready room name
	private const string m_readyRoomScene = "ReadyRoom";
	
	private bool m_enteringMatch = false;//Are we or have we entered a match?
	public GameObject m_avatarPrefab;//The prefab to spawn for us/over the network
	private string m_playerName;//Current player name
	
	
	private PhotonView m_photonView;
	
	//Game Play stuff
	private bool m_isReadyRoom;//Are we in the ready room?
	
	private int m_minPlayerCount = 2;//Min number of players for play to start or continue
	private	float m_respawnWait = 3.0f;//How long does a player need to wait to start?
	
	private PhotonView m_view;//The photonView
	
	private string 		m_wantJoinRoomName = "", m_wantCreateRoomName = "";
	private bool		m_wantsToJoinRoom, m_wantsToCreateRoom;
	
	private CreateRoomSettings m_createRoomSettings;
	
	private MCTimer	m_endMatchSummaryTimer;
	
	//End of match game summary
	private bool m_matchEndSummaryActive = false, m_headingToReadyRoom = false;
	
	
	//Final setup before first Update()
	void Start()
	{
		m_view = PhotonView.Get( this );
		//InvokeRepeating("CheckplayerID", 1, 5);
	}
	
	void CheckplayerID()
	{
		Debug.Log("player id is : " + PlayerStatistics.instance.playerID);
	}
	
	//Immediate setup
	void Awake(){
		InitializeMessaging();
	}
	
	private void InitializeMessaging(){
		GameObject.DontDestroyOnLoad( gameObject );
		//ReadyRoom.instance.enabled = false;
		Messenger.AddListener< string >( "SetPlayerName", SetPlayerName );
		Messenger.AddListener<string>( "JoinRoom", JoinRoom );
		Messenger.AddListener<CreateRoomSettings>( "CreateRoom", CreateRoom );
		
		Messenger.MarkAsPermanent( "SetPlayerName" );
		Messenger.MarkAsPermanent( "JoinRoom" );
		Messenger.MarkAsPermanent( "CreateRoom" );
	}
	
	//Set the local player instance name
	public void SetPlayerName( string name ){
		m_playerName = name;
		PhotonNetwork.player.name = name;
	}
	
	//players ready to launch?
	private bool IsAllPlayersReadyForLaunch(){
		return true;
	}
	
	//Can we start the match?
	private bool IsAllPlayersReadyForStart(){

		return true;
	}
	
	//Update master client logic - Only called if we actually are the master client
	public void Update(){
		if( !PhotonNetwork.connected )
			return;
		if(PhotonNetwork.connected)
			StartCoroutine(UpdateIfMasterClient());
		
		//CHAT
		//If the chat visualizer has a message, we pull it and send
		string chatMessage = ChatVisualizer.messageToSend;
		if( chatMessage != "" && PhotonNetwork.connectionState == ConnectionState.Connected)
		{
			m_view.RPC( "TransferChatMessage",PhotonTargets.All, chatMessage );
		}
	
		//Debug.Log("MenulabelNameyanameofmenulabel    " +  menuLabelName);
		//if(Application.loadedLevelName == "ReadyRoom")
			//Debug.Log(dayVotes+"   "+sunsetVotes+"  "+nightVotes);
	}
		
	private string GetLevelToLoadFromVotes()
	{
		for(int i = 0; i < subScenes.Length ; i++)
		{
			if(ReadyRoom.instance.levelToLoad == subScenes[i].headName)
			{
				Debug.Log("GetVotesResult() : " + GetVotesResult());
				switch(GetVotesResult())
				{
					case "Day":
						return subScenes[i].daySceneName;

					case "Sunset":
						return subScenes[i].sunsetSceneName;

					case "Night":
						return subScenes[i].nightSceneName;

					default:
					 	return "";
				}
			}
		}
		return "";	
	}

	string GetVotesResult()
	{
		string[] typeArray1 = new string[]{"Day","Sunset","Night"};
		string[] typeArray2 = new string[]{"Day","Sunset"};
		string[] typeArray3 = new string[]{"Day","Night"};
		string[] typeArray4 = new string[]{"Sunset","Night"};
		
		if(dayVotes == sunsetVotes && dayVotes == nightVotes)
			return typeArray1[UnityEngine.Random.Range(0,typeArray1.Length)];
		else if(dayVotes > sunsetVotes && dayVotes > nightVotes)
			return typeArray1[0];
		else if(sunsetVotes > dayVotes && sunsetVotes > nightVotes)
			return typeArray1[1];
		else if(nightVotes > dayVotes && nightVotes > sunsetVotes)
			return typeArray1[2];
		else if(dayVotes == sunsetVotes && dayVotes > nightVotes)
			return typeArray2[UnityEngine.Random.Range(0,typeArray2.Length)];
		else if(dayVotes == nightVotes && dayVotes > sunsetVotes)
			return typeArray3[UnityEngine.Random.Range(0,typeArray3.Length)];
		else if(sunsetVotes == nightVotes && sunsetVotes > dayVotes)
			return typeArray4[UnityEngine.Random.Range(0,typeArray4.Length)];
		else
			return typeArray1[UnityEngine.Random.Range(0,typeArray1.Length)];
	}
	
	
	/**************************
	 * Chat Tasks
	 **************************/
	public void NotifyAboutChat(string text)
	{
		string myText;
		myText = PhotonNetwork.player.name+" : "+text;
		m_photonView.RPC( "UpdateUserChat",PhotonTargets.Others,myText);
	}
	
	[RPC]
	private void UpdateUserChat(string text)
	{
		GameObject.FindWithTag("ChatManager").GetComponent<ChatInput>().OnSubmitOtherUserChat(text);
	}
	
	public void PlayDeadAudio(PhotonPlayer killer)
	{
		m_photonView.RPC("PlayerDeadAudio", PhotonTargets.Others, killer);
	}
	
	[RPC]
	void PlayerDeadAudio(PhotonPlayer killer)
	{
		Debug.Log("HAAAA DEAD");
		
		Debug.Log("sender name " + killer.name);
		Debug.Log("player name " + PhotonNetwork.player.name);
		
		if(killer != null)
		{
			if(killer != PhotonNetwork.player && !audio.isPlaying)
			{
				Debug.Log("TIME TO PLAY SOUND");
				if(!gameObject.GetComponent<AudioListener>())
					gameObject.AddComponent("AudioListener");
				
				PlaySound(playerDeadBoomAudio);
				//audio.PlayOneShot(playerDeadBoomAudio);
			
				StartCoroutine(DestroyListenerAfterYield(playerDeadBoomAudio.length));
			}
		}
	}
	
	public IEnumerator SetCustomMapCoordinates()
	{
		// reset the positions first
		bunkerNamesArray = null;
		bunkerPositionsArray = null;
		bunkerRotationsArray = null;
		
		Debug.Log("Custom Map Data Recieved");
		yield return StartCoroutine(CustomMapEditor.SetMapElements(customMapName));
		bunkerNamesArray = new string[CustomMapEditor.bunkersCount];
		bunkerPositionsArray = new Vector3[CustomMapEditor.bunkersCount];
		bunkerRotationsArray = new Vector3[CustomMapEditor.bunkersCount];
		
		bunkerNamesArray = CustomMapEditor.bunkerNamesArray;
		bunkerPositionsArray = CustomMapEditor.bunkerPositionsArray;
		bunkerRotationsArray = CustomMapEditor.bunkerRotationsArray;
		
//		bunkerNamesArray = new string[XmlEditor.GetNumberOfElementsInMap(customMapName)];
//		bunkerPositionsArray = new Vector3[XmlEditor.GetNumberOfElementsInMap(customMapName)];
//		bunkerRotationsArray = new Vector3[XmlEditor.GetNumberOfElementsInMap(customMapName)];
//		//string tempo = "";
//		for(int i = 0; i < XmlEditor.GetNumberOfElementsInMap(customMapName); i++)
//		{			
//			bunkerNamesArray[i] = (XmlEditor.GetType(customMapName,"Bunker"+i,"BunkerType"));
//			bunkerPositionsArray[i] = (XmlEditor.GetTranslation(customMapName,"Bunker"+i,"Position"));
//			bunkerRotationsArray[i] =(XmlEditor.GetTranslation(customMapName,"Bunker"+i,"Rotation"));
//		}
	}
	
	[RPC]
	public void MapDataOverNetwotk(string[] nameList,Vector3[] positionList,Vector3[] rotationList)
	{	
		bunkerNamesArray = nameList;
		bunkerPositionsArray = positionList;
		bunkerRotationsArray = rotationList;
		
		Debug.Log(bunkerNamesArray.Length);
		
		for(int i=0;i<bunkerNamesArray.Length;i++)
			Debug.Log(bunkerNamesArray[i]);
		
		GameObject m_fieldLoader = GameObject.Find("field1");
		if(m_fieldLoader)
		{
			if(!m_fieldLoader.GetComponent<FieldLoader>().IsMapSet())
				m_fieldLoader.GetComponent<FieldLoader>().PlaceBunkers();
		}	
	}
	
	// methods to convert data to binary format before sending over the network
	private string CompressData(List<string> m_list)
	{
		MemoryStream m_stream = new MemoryStream();
		BinaryFormatter m_formatter = new BinaryFormatter();
		m_formatter.Serialize(m_stream,m_list);
		string data = Convert.ToBase64String(m_stream.GetBuffer());
		Debug.Log(data);
		return data;
	}
	
	
	private string CompressData(List<Vector3> m_list)
	{
		MemoryStream m_stream = new MemoryStream();
		BinaryFormatter m_formatter = new BinaryFormatter();
		m_formatter.Serialize(m_stream,m_list);
		string data = Convert.ToBase64String(m_stream.GetBuffer());
		Debug.Log(data);
		return data;
	}
	
	public List<Vector3> DeCompressData(string data,bool isVector)
	{
		MemoryStream m_stream = new MemoryStream(Convert.FromBase64String(data));
		BinaryFormatter m_formatter = new BinaryFormatter();
		List<Vector3>  m_list = new List<Vector3>();
		m_list = (List<Vector3>)m_formatter.Deserialize(m_stream);
		return m_list;
		
	}
	
	public List<string> DeCompressData(string data)
	{
		MemoryStream m_stream = new MemoryStream(Convert.FromBase64String(data));
		BinaryFormatter m_formatter = new BinaryFormatter();
		List<string>  m_list = new List<string>();
		m_list = (List<string>)m_formatter.Deserialize(m_stream);
		return m_list;
	}
	
	
	private IEnumerator UpdateIfMasterClient(){
		if( PhotonNetwork.isMasterClient ){
						
			if( RoomManager.state == NetRoomState.InReadyRoom ){//Ask room if we're in ready room state
				if( RoomIsReadyForLaunch() ){//Are all players ready/min player count met?
					//Prepare for future ReadyRoom use
					ClearPlayerReadyFlags();
					
					RoomManager.waitingForPlayers = true;
					
					// check for number of votes and change the level
					ReadyRoom.instance.levelToLoad = GetLevelToLoadFromVotes();
					
					//Tell everyone we're launching the match
					Debug.Log("ReadyRoom.instance.levelToLoad : " + ReadyRoom.instance.levelToLoad);
					yield return StartCoroutine(SetCustomMapCoordinates());
					
					m_photonView.RPC("MapDataOverNetwotk",PhotonTargets.Others,bunkerNamesArray,bunkerPositionsArray,bunkerRotationsArray);
					m_photonView.RPC( "EnteringMatch", PhotonTargets.All, ReadyRoom.instance.levelToLoad );
					RoomManager.SetRoomLevel(ReadyRoom.instance.levelToLoad);
					PlayerManager.PreparePlayersForMatch();
					//GameObject.Destroy( gameObject );
					RoomManager.state = NetRoomState.InMatch;
					
					//Hashtable table = new Hashtable();
					//table.Add( "IsReadyRoom", false );//ReadyRoom status.  Are we in the ready room?  Default to true
							
					//PhotonNetwork.room.SetCustomProperties( table );
					//Application.LoadLevel( "TestMatchScene" );
				}
			}
			
			//We're in a match, update for that
			if( RoomManager.state == NetRoomState.InMatch ){
				if( RoomManager.waitingForPlayers ){//Must be waiting for the match to start, lets check if they are ready
					if( DoAllPlayersReportReady() ){
						RoomManager.waitingForPlayers = false;
						PlayerManager.ClearPlayerIsReadyFlags();
					}
				}
			}
			
			//manage end of match stuff
			UpdateEndGameSummaryState();
			
		}		
	}
	
	//Setup for the match, instantiate our NetPlayer game object, let it do it's thing until NetMatch detects game over
	private void SetupForMatch(){
		
		GameObject myPlayer = PhotonNetwork.Instantiate( m_avatarPrefab.name, new Vector3(0,15,0), Quaternion.identity, 0 );
		myPlayer.GetComponent<vp_FPController>().enabled = true;
		myPlayer.GetComponent<vp_FPInput>().enabled = true;
		myPlayer.GetComponent<vp_FPWeaponHandler>().enabled = true;
		myPlayer.GetComponent<vp_FPPlayerEventHandler>().enabled = true;
		myPlayer.GetComponent<vp_PlayerDamageHandler>().enabled = true;			
		myPlayer.GetComponent<vp_SimpleInventory>().enabled = true;			
		myPlayer.GetComponent<vp_SimpleHUD>().enabled = true;			
		myPlayer.GetComponent<vp_SimpleCrosshair>().enabled = true;			
		myPlayer.GetComponent<AudioListener>().enabled = true;			
		myPlayer.GetComponent<GunAnimationSetup>().enabled = true;	
		myPlayer.GetComponent<PlayerShootSetup>().enabled = true;
		myPlayer.transform.FindChild("MaleSoldier/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1").GetComponent<CharacterBodyModifier>().enabled = true;	
	    myPlayer.transform.FindChild("FPSCamera").gameObject.SetActive(true);
		SetLayerRecursively(myPlayer.transform.FindChild("FPSCamera").gameObject,9);
		SetLayerRecursively(myPlayer.transform.FindChild("MaleSoldier").gameObject,8);		
				
		ChatVisualizer.instance.autoDisplay = true;
		GameSummary.instance.display = false;
		Debug.Log("Player Instantiated");
	}
	
	private void SetLayerRecursively(GameObject child,int layer){
		child.gameObject.layer = layer;
		if(child == null){
			return;
		}
		else{
			foreach(Transform children in child.transform){
				children.gameObject.layer = layer;
				SetLayerRecursively(children.gameObject,layer);
			}
		}
	}
	
	private void UpdateEndGameSummaryState(){
		
		if( m_endMatchSummaryTimer != null ){
			if( m_endMatchSummaryTimer.Run() )
			{
				m_endMatchSummaryTimer = null;	
			}
			
			GameSummary.instance.enabled = true;
			GameSummary.instance.display = true;
		}
		else{
//			Debug.Log("JSUSTGSHJFGGHSFGHSF" + m_matchEndSummaryActive);
			if( m_matchEndSummaryActive ){
				m_matchEndSummaryActive = false;
				if( PhotonNetwork.isMasterClient )
				{
					Screen.showCursor = true;
					Screen.lockCursor = false;
					RoomManager.state = NetRoomState.InReadyRoom;
					m_view.RPC( "ReturnToReadyRoom", PhotonTargets.All );
					//ReturnToReadyRoom();
				}
			}
		}
		
	}
	
	//////////////////////////////
	//PHOTON/UNITY EVENTS
	//////////////////////////////
	
	void OnCreatedRoom(){
		Debug.Log( "Created a room" );
		
		ResetVotes();
		//Enable the ready room GUI
		ReadyRoom.instance.enabled = true;

		//Set defaults
		RoomManager.state = NetRoomState.InReadyRoom;
		RoomManager.minPlayerCount = m_minPlayers;
		RoomManager.respawnWait = m_createRoomSettings.m_respawnWait;// players have to wait 5 seconds to respawn by default
		RoomManager.gameType = m_createRoomSettings.m_gameType;
		RoomManager.scoreLimit = m_createRoomSettings.m_scoreLimit;
		
		PhotonNetwork.room.visible = true;
	}
	
	//We joined a room, if the room is still in the ready room state, do the same, if they are in match, bypass the ready room
	void OnJoinedRoom(){

		Debug.Log( "Joined a game room" );
		Debug.Log( "Current room state: " + NetRoomState.InReadyRoom );
		//Is everyone in the ready room?
		if( RoomManager.state == NetRoomState.InReadyRoom ){
			
			ReadyRoom.instance.enabled = true;
			
		}
		else if( RoomManager.state == NetRoomState.InMatch ){//Not in ready room, enter the game
			
			//Here we probably need to do a check to see if we have the level loaded for the match
			//It's possible that the room readied up and launched while we were connecting
			//So by the time we get here, we might be SOL :)
			
			Debug.Log( "Room is in match, setup for the match" );
			PlayerManager.PreparePlayer( PhotonNetwork.player );
			SetupForMatch();
		}
	}
	
	void OnPhotonPlayerConnected( PhotonPlayer player ){
		
		Debug.Log( "A player has connected. Player name: " + player.name);
		
		if(!gameObject.GetComponent<AudioListener>())
			gameObject.AddComponent("AudioListener");
		
		//if(audio.isPlaying)
		//	PlaySound(playerJoinedAudio);
		audio.PlayOneShot(playerJoinedAudio);
		
		StartCoroutine(DestroyListenerAfterYield(playerJoinedAudio.length));
		
		if( RoomManager.state == NetRoomState.InMatch )
		{
			m_photonView.RPC("MapDataOverNetwotk",PhotonTargets.Others,bunkerNamesArray,bunkerPositionsArray,bunkerRotationsArray);
			PlayerManager.PreparePlayer( player );	
		}
	}	
	
	IEnumerator DestroyListenerAfterYield(float dt)
	{
		yield return new WaitForSeconds(dt);
		if(gameObject.GetComponent<AudioListener>())
			Destroy(gameObject.GetComponent<AudioListener>());
	}
	
	void OnDisconnectedFromPhoton()
	{
		//Return to the main menu if we disconnect from photon
/*		if(Application.loadedLevelName != "LoginMenu")
		Application.LoadLevel( "LoginMenu" );*/
		Debug.Log("OnDisconnectedFromPhoton");
	}
	
	void OnLeftRoom(){
		//Return to the main menu if we leave a room
		Application.LoadLevel( "LoginMenu" );
		Debug.Log("onleftRoom");
		
	}
	
	void OnPhotonPlayerDisconnected( PhotonPlayer player ){
		
		if( PhotonNetwork.isMasterClient ){
			PhotonNetwork.RemoveRPCs( player );	
		}
		Debug.Log("OnPhotonPlayerDisconnected");
	}
	
	
	
	//If a level was loaded it means we're preparing to enter a game one way or another.
	void OnLevelWasLoaded(){
		
		DacalReceiver.ClearInternalList();
		//NetPlayer.ClearPlayerList();
		
		//We were joining a room which prompted the level load
		if( m_wantsToJoinRoom ){
			Debug.Log( "Level loaded to join room in session" );
			
			m_wantsToJoinRoom = false;
			PhotonNetwork.JoinRoom( m_wantJoinRoomName );
		}
		
		if( m_wantsToCreateRoom ){
			Debug.Log( "Level was loaded to create room" );
			
			m_wantsToCreateRoom = false;
			
			Hashtable table = new Hashtable();
            table.Add("LevelName", ReadyRoom.instance.levelToLoad);
			table.Add( "GameState", (int)NetRoomState.InReadyRoom );
			table.Add("TimeLimit", (float)m_createRoomSettings.m_timeLimit);
			
			string[] list = {"LevelName", "GameState"};
			
			PhotonNetwork.CreateRoom( m_createRoomSettings.m_roomName, true, true, m_createRoomSettings.m_maxPlayers, table, list );
		}
		
		if( m_headingToReadyRoom ){
			Debug.Log( "Entered ready room after match end" );
			
			m_headingToReadyRoom = false;
			
			ReadyRoom.instance.enabled = true;
		}
	
		if( m_enteringMatch ){
			Debug.Log( "Level loaded to join room in session" );
			
			m_enteringMatch = false;
			SetupForMatch();
		}
		//We were creating a room which prompted the level load

	}
	
	//////////////////////////////
	//RPC METHODS
	//////////////////////////////
	//Master cient informs us that we are now entering the match
	[RPC]
	void EnteringMatch( string map ){
		if( !m_enteringMatch ){
			m_enteringMatch = true;
			
			Debug.Log("map name is  =="+map);
			//Here we need to load the level and then when that is done, instantiate our guy
            //Application.LoadLevel(map);
			Application.LoadLevel(2);//FOR TESTING PURPOSE
			//SetupForMatch();
		}
	}
	
	[RPC]
	void TransferChatMessage( string message, PhotonMessageInfo info ){
		//lets see if the message is preceded by any number of commands
		string finalMessage = info.sender.name+": " + message;
		ChatVisualizer.AddMessage( finalMessage );
		if( info.sender == PhotonNetwork.player )
			ChatVisualizer.newLocalmessage = true;
	}
	
	[RPC]
	void ReturnToReadyRoom(){
		
		Debug.Log("RPC THROWN");
		m_headingToReadyRoom = true;
		
		Screen.showCursor = true;
		Screen.lockCursor = false;
		
		m_endMatchSummaryTimer = null;
		
		if( PhotonNetwork.isMasterClient ){
			//PhotonNetwork.RemoveRPCsInGroup( 0 );
			CleanUpMatchData();
		}
		else
		{
			StartEndGameSummary();
		}
		
		m_matchEndSummaryActive = false;
		//LoadReadyRoomScene();
	}
	
	//***********************************
	//MESSENGER METHODS
	//***********************************	

	//We want to join a room, load the level
	void JoinRoom( string _name ){
		m_wantJoinRoomName = _name;
		m_wantsToJoinRoom = true;
		
		string 	map = "";
		bool	inMatch = false;
		foreach( RoomInfo room in PhotonNetwork.GetRoomList() ){
			Debug.Log("Room name comparisons    "+room.name+"     "+_name);
			if( room.name == _name )
			{
				Debug.Log( "Found the room" );
				map = RoomManager.GetRoomLevel( room );
				inMatch = RoomManager.GetRoomState( room ) == NetRoomState.InMatch;
				break;
			}
		}
		
		Debug.Log("map name is  ====="+map );
		Debug.Log("inMatch = " + inMatch );
		if( !inMatch )
		{
			//Application.LoadLevel( m_readyRoomScene );
			Application.LoadLevel(2);//FOR TESTING PURPOSES
		}
		else
		{
			//Application.LoadLevel(map);
			Application.LoadLevel(2);//FOR TESTING PURPOSES
		}
            
	}
	
	//We want to create a room, load the level
	void CreateRoom( CreateRoomSettings settings ){
		m_wantsToCreateRoom = true;
		m_createRoomSettings = settings;
		Debug.Log("came here");
		Application.LoadLevel( m_readyRoomScene );
		                         
		//Debug.Log( m_wantCreateRoomName );
		//Application.LoadLevel( "TestMatchScene" );
	}
	
	//We want to change the map, load the level(Unused)
	void ChangeMap( string mapName ){
		m_wantCreateRoomName = name;
		m_wantsToCreateRoom = true;

        Application.LoadLevel(mapName);
	}
	
	//***********************************
	//STATIC METHODS
	//***********************************	
	
	
	//Clear the launch flags on all players, used in the ready toom to see if we are prepared to launch
	public void ClearPlayerReadyFlags(){
	
		if( PhotonNetwork.player.isMasterClient ){
			
			for( int i = 0; i < PhotonNetwork.playerList.Length; i++ ){
				PlayerManager.SetIsReady( PhotonNetwork.playerList[ i ], false );			
			}
			
		}
		
	}
	
	/////////////////////////////////////////////////////////
	//////////// Start Voting Functionality  ////////////
	/////////////////////////////////////////////////////////
	public void UpdateUserVote(string voteType)
	{
		if(PhotonNetwork.isMasterClient)
		{
			Debug.Log("Vote casted by server");
			CastMyVote(voteType);
		}
		else
		{
			Debug.Log("Vote casted by client");
			m_photonView.RPC( "CastMyVote", PhotonTargets.MasterClient,voteType);
		}
	}
	
	[RPC]
	public void CastMyVote(string voteType)
	{
		switch(voteType)
		{
			case "Day":
				dayVotes++;
				break;
			case "Sunset":
				sunsetVotes++;
				break;
			case "Night":
				nightVotes++;
				break;
		}
	}
	
	public void ResetVotes()
	{
		if(PhotonNetwork.player.isMasterClient)
		{
			Debug.Log("Votes Reset");
			dayVotes = 0;
			sunsetVotes = 0;
			nightVotes = 0;
		}
		else
		{
			Debug.Log("Votes not reset as called from client server");
		}
	}
	
	/////////////////////////////////////////////////////////
	//////////// End Voting Functionality  ////////////
	/////////////////////////////////////////////////////////
	
	//Does the room meet min player requirements to launch a match?
	public bool RoomHasMinPlayerCount(){
		if( RoomManager.minPlayerCount <= PhotonNetwork.room.playerCount ){
			return true;
		}
		
		return false;
	}
	
	//Are all players ready to launch? Right number of players and all have said they are ready.
	public bool RoomIsReadyForLaunch(){
		
		if( !RoomHasMinPlayerCount() ){
			Debug.Log( "Min player count not met." );
			return false;
		}

		for( int i = 0; i<PhotonNetwork.playerList.Length; i++ ){

			if( !PlayerManager.GetIsReady( PhotonNetwork.playerList[ i ] ) ){
				//Debug.Log( "All players not ready." );
				return false;
			}			
		
		}
		
		return true;
	}
	
	//Clean up all match data and load the ready room scene
	private void CleanUpMatchData(){
		
		if( PhotonNetwork.isMasterClient ){
			
			PhotonNetwork.RemoveRPCsInGroup( 0 );
			//PhotonNetwork.RemoveAllInstantiatedObjects();
			PhotonNetwork.DestroyAll();
		}
		
	}
	
	private void LoadReadyRoomScene()
	{
		Application.LoadLevel( m_readyRoomScene );
	}
	
	
	
	public void StartEndGameSummary()
	{
		Debug.Log("1");
		if( !m_matchEndSummaryActive )
		{
			Debug.Log("2");
			m_matchEndSummaryActive = true;
			m_endMatchSummaryTimer = new MCTimer( 0.5f );
//			AudioListener[] myListeners = FindObjectsOfType(typeof(AudioListener)) as AudioListener[];
//			
//			if(myListeners.Length == 0)
			if(!gameObject.GetComponent<AudioListener>())
				gameObject.AddComponent("AudioListener");
			
			//if(!audio.isPlaying)
			//	PlaySound(endgameAudio);
			audio.PlayOneShot(endgameAudio);
			
			StartCoroutine(DestroyListenerAfterYield(endgameAudio.length));
			
			GameObject tmpcam = new GameObject("tmpcam");
			tmpcam.AddComponent<Camera>();
			tmpcam.camera.cullingMask &= ~( 1 << LayerMask.NameToLayer("ScoreBoard"));
			tmpcam.transform.position = new Vector3(0,72,0);
			ScoreBoard.instanceGO.transform.GetComponentInChildren<Camera>().orthographicSize = 1.0f;
			ScoreBoard.instanceGO.transform.GetComponentInChildren<UICamera>().useMouse = true;
			ScoreBoard.instanceGO.transform.Find("Camera/Anchor/Panel/MainMenuBtn").gameObject.GetComponent<UILabel>().color = new Color(1.0f, 0.3f, 0.3f, 1.0f);
			ScoreBoard.instanceGO.transform.parent = tmpcam.transform;
			ScoreBoard.instanceGO.transform.localPosition = Vector3.zero;
			ScoreBoard.instanceGO.transform.localScale = new Vector3(2,2,2);
			ScoreBoard.instance.showScoreboard = true;
			
			SaveMatchResult();
			//CleanUpMatchData();
			NetMatch.Release();
			HUD.instance.display = false;
			GameSummary.instance.forceEndGameDisplay = true;
//			foreach (var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
//			{
//    			if(gameObj.name == "FPSPlayer(Clone)")
//    			{
//					Destroy(gameObj);
//        			//Do something...
//   				}
//			}
		}
	}
	
	private void SaveMatchResult()
	{
		StartCoroutine(PlayerStatistics.instance.SendData("Kills",(PlayerManager.GetKillCount(PhotonNetwork.player) + PlayerStatistics.instance.kills),true));
		StartCoroutine(PlayerStatistics.instance.SendData("Deaths", (PlayerManager.GetDeathCount(PhotonNetwork.player) + PlayerStatistics.instance.deaths),true));
		StartCoroutine(PlayerStatistics.instance.SendData("total_pp",(PlayerManager.GetPlayerPoints(PhotonNetwork.player) + PlayerStatistics.instance.pPoints),true));
	}
	
	private bool DoAllPlayersReportReady(){
		bool allReady = true;
		for( int i = 0 ;i < PhotonNetwork.playerList.Length; i++ ){
			if( !PlayerManager.GetIsReady( PhotonNetwork.playerList[i] ) ){
				allReady = false;	
			}
		}
		
		return allReady;
	}
	
	void PlaySound(AudioClip sound)
	{
		audio.clip = sound;
		//audio.volume = vol;
		audio.Play();
	}
}