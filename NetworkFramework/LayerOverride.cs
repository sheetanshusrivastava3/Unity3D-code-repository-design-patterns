using UnityEngine;
using System.Collections;

public class LayerOverride : MonoBehaviour {
	
	public int m_layer = 8;
	
	// Use this for initialization
	void Start () {
		gameObject.layer = m_layer;	
	}

}
