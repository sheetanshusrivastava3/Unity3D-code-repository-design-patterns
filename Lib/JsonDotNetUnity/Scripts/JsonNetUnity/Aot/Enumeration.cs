﻿#if (UNITY_IOS || UNITY_IPHONE)
using System;
using System.Linq;
using System.Collections;

namespace Newtonsoft.Json.Aot
{
	public class Enumeration
	{
		//Delegate to return IEnumerator
		private delegate IEnumerator GetEnumerator();

		public static void ForEach<T>(object enumerable, Action<T> action)
		{
			if (enumerable == null)
				return;

			var listType = enumerable.GetType().GetInterfaces().First(x => !(x.IsGenericType) && x == typeof(IEnumerable));

			if (listType == null)
				throw new ArgumentException("Object does not implement IEnumerable interface", "enumerable");

			var method = listType.GetMethod("GetEnumerator");

			if (method == null)
				throw new InvalidOperationException("Failed to get 'GetEnumerator()' method info from IEnumerable type");

			IEnumerator enumerator = null;

			try
			{
				//Create a delegate instance to get the enumerator
				var enumeratorDelegate = (GetEnumerator)Delegate.CreateDelegate(typeof(GetEnumerator), enumerable, method);


				//Create the enumerator by executing the delegate.  
				//This is much faster than using Invoke
				enumerator = enumeratorDelegate();

				if (enumerator != null)
				{
					while (enumerator.MoveNext())
					{
						action((T)enumerator.Current);
					}
				}
				else
				{
					UnityEngine.Debug.Log("GetEnumerator() returned null.");
				}
			}
			finally
			{
				var disposable = enumerator as IDisposable;

				if (disposable != null)
					disposable.Dispose();
			}
		}
	}
}
#endif