using System.IO;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Converters;
using UnityEngine;
using System;
using Newtonsoft.Json;

public class SerializationComponent : MonoBehaviour
{
	#region Display Label Fields
	public GUIText Label1;
	public GUIText Label2;
	public GUIText Label3;
	#endregion

	private JsonTestType _lastTestType;
	public JsonTestType TestType;

	// Use this for initialization
	void Start()
	{
		_lastTestType = TestType;
		RunTest();
	}

	void Update()
	{
		if (TestType != _lastTestType)
		{
			_lastTestType = TestType;
			RunTest();
		}
	}

	#region Code Examples
	private void RunTest()
	{
		switch (TestType)
		{
			case JsonTestType.JsonSerialization:
				JsonExample();
				break;
			case JsonTestType.BsonSerialization:
				BsonExample();
				break;
			case JsonTestType.EnumAsNameSerialization:
				EnumAsNameExample();
				break;
			case JsonTestType.EnumAsValueSerialization:
				EnumAsValueExample();
				break;
			case JsonTestType.VectorSerialization:
				VectorSerializationExample();
				break;
			case JsonTestType.VectorNullableSerialization:
				VectorSerializationNullableExample();
				break;
		}
	}

	private void JsonExample()
	{
		var obj = new SampleObject
						{
							StringProperty = "JsonExample",
							FloatProperty = 10.234567f,
							GuidProperty = Guid.NewGuid(),
							VectorProperty = new Vector3(10.0f, 20.1f, 30.2f)
						};

		Label1.text = string.Format("{0}, {1}, {2}, ({3}, {4}, {5})",
										obj.StringProperty, obj.FloatProperty,
										obj.GuidProperty.ToString(), obj.VectorProperty.x,
										obj.VectorProperty.y, obj.VectorProperty.z);

		var serializedSample = JsonConvert.SerializeObject(obj);
		Label2.text = serializedSample;

		var desObj = JsonConvert.DeserializeObject<SampleObject>(serializedSample);

		Label3.text = string.Format("{0}, {1}, {2}, ({3}, {4}, {5})",
										desObj.StringProperty, desObj.FloatProperty,
										desObj.GuidProperty.ToString(), desObj.VectorProperty.x,
										desObj.VectorProperty.y, desObj.VectorProperty.z);

	}

	private void BsonExample()
	{
		var obj = new SampleObject
		{
			StringProperty = "JsonExample",
			FloatProperty = 10.234567f,
			GuidProperty = Guid.NewGuid(),
			VectorProperty = new Vector3(10.0f, 20.1f, 30.2f)
		};

		Label1.text = string.Format("{0}, {1}, {2}, ({3}, {4}, {5})",
										obj.StringProperty, obj.FloatProperty,
										obj.GuidProperty.ToString(), obj.VectorProperty.x,
										obj.VectorProperty.y, obj.VectorProperty.z);

		var ms = new MemoryStream();
		var serializer = new JsonSerializer();

		var writer = new BsonWriter(ms);
		serializer.Serialize(writer, obj);

		Label2.text = BitConverter.ToString(ms.ToArray());

		ms.Seek(0, SeekOrigin.Begin);

		var reader = new BsonReader(ms);

		var desObj = serializer.Deserialize<SampleObject>(reader);

		Label3.text = string.Format("{0}, {1}, {2}, ({3}, {4}, {5})",
										desObj.StringProperty, desObj.FloatProperty.ToString(),
										desObj.GuidProperty.ToString(), desObj.VectorProperty.x,
										desObj.VectorProperty.y, desObj.VectorProperty.z);
	}

	private void EnumAsNameExample()
	{
		var obj = JsonTestType.EnumAsNameSerialization;
		Label1.text = obj.ToString();

		var serialized = JsonConvert.SerializeObject(obj);
		Label2.text = serialized;

		var desObj = JsonConvert.DeserializeObject<JsonTestType>(serialized);
		Label3.text = desObj.ToString();
	}

	private void EnumAsValueExample()
	{
		
		var obj = JsonTestType.EnumAsValueSerialization;
		Label1.text = obj.ToString();

		var serialized = JsonConvert.SerializeObject(obj, new StringEnumConverter());
		Label2.text = serialized;

		var desObj = JsonConvert.DeserializeObject<JsonTestType>(serialized);
		Label3.text = desObj.ToString();
	}

	private void VectorSerializationExample()
	{
		//Vector2, Vector3 and Vector4 are Serialized and 
		//ReferenceLoopHandling is set Automatically.

		var obj = new Vector3(10.0f, 20.2f, 30.3f);
		Label1.text = obj.ToString();

		var serialized = JsonConvert.SerializeObject(obj);
		Label2.text = serialized;

		var desObj = JsonConvert.DeserializeObject<Vector3>(serialized);
		Label3.text = desObj.ToString();
	}

	private void VectorSerializationNullableExample()
	{
		//Nullable Vectors and objects supported as well
		var obj = new Vector3?(new Vector3(40f, 50f, 60f));
		Label1.text = obj.ToString();

		var serialized = JsonConvert.SerializeObject(obj);
		Label2.text = serialized;

		var desObj = JsonConvert.DeserializeObject<Vector3?>(serialized);

		//You could even deserialize from nullable to value type by using this instead:
		//var desObj = JsonConvert.DeserializeObject<Vector3>(serialized);

		Label3.text = desObj.ToString();
	}
	#endregion


}

#region TEST TYPE ENUMERATION
public enum JsonTestType
{
	JsonSerialization,
	BsonSerialization,
	EnumAsNameSerialization,
	EnumAsValueSerialization,
	VectorSerialization,
	VectorNullableSerialization
}
#endregion