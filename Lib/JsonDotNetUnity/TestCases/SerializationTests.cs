using System;
using System.Collections.Generic;
using System.Text;
using SampleClassLibrary;
using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using Assets.DustinHorne.JsonDotNetUnity.TestCases;
using Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels;


public class SerializationTests : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Test1();
		Test2();
		Test3();
		Test4();
	}

	//Simple vector3 serialization
	private void Test1()
	{
		LogStart(1);

		var v = new Vector3(2, 4, 6);
		var serialized = JsonConvert.SerializeObject(v);
		Log(serialized);
		var v2 = JsonConvert.DeserializeObject<Vector3>(serialized);

		LogResult("4", v2.y);

		LogEnd(1);
	}

	//List<T> serialization
	private void Test2()
	{
		LogStart(2);

		var objList = new List<SimpleClassObject>();

		for (var i = 0; i < 4; i++)
		{
			objList.Add(TestCaseUtils.GetSimpleClassObject());
		}

		var serialized = JsonConvert.SerializeObject(objList);
		LogSerialized(serialized);

		var newList = JsonConvert.DeserializeObject<List<SimpleClassObject>>(serialized);

		LogResult(objList.Count.ToString(), newList.Count);
		LogResult(objList[2].TextValue, newList[2].TextValue);

		LogEnd(2);
	}

	//TypeNameHandling with Polymorphism
	private void Test3()
	{
		LogStart(3);

		var list = new List<SampleBase>();

		for (var i = 0; i < 4; i++)
		{
			list.Add(TestCaseUtils.GetSampleChid());
		}

		var serialized = JsonConvert.SerializeObject(list, Formatting.None, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });
		LogSerialized(serialized);

		var newList = JsonConvert.DeserializeObject<List<SampleBase>>(serialized);
		LogResult(list[2].TextValue, newList[2].TextValue);

		LogEnd(3);
	}

	//Testing serialization of a class from an external class library
	private void Test4()
	{
		LogStart(4);
		
		var o = new SampleExternalClass {SampleString = Guid.NewGuid().ToString()};

		o.SampleDictionary.Add(1, "A");
		o.SampleDictionary.Add(2, "B");
		o.SampleDictionary.Add(3, "C");
		o.SampleDictionary.Add(4, "D");

		var serialized = JsonConvert.SerializeObject(o);
		LogSerialized(serialized);

		var newObj = JsonConvert.DeserializeObject<SampleExternalClass>(serialized);

		LogResult(o.SampleString, newObj.SampleString);
		LogResult(o.SampleDictionary.Count.ToString(), newObj.SampleDictionary.Count);

		var keys = new StringBuilder(4);
		var vals = new StringBuilder(4);

		foreach (var kvp in o.SampleDictionary)
		{
			keys.Append(kvp.Key.ToString());
			vals.Append(kvp.Value);
		}

		LogResult("1234", keys.ToString());
		LogResult("ABCD", vals.ToString());
	}


	private void LogStart(int testNum)
	{
		Log(string.Format("====== SERIALIZATION TEST #{0} STARTING", testNum));
	}

	private void LogEnd(int testNum)
	{
		Log(string.Format("====== SERIALIZATION TEST #{0} COMPLETE", testNum));
	}

	private void Log(object message)
	{
		Debug.Log(message);
	}

	private void LogSerialized(string message)
	{
		Debug.Log("^v^v^v SERIALZED OBJECT ^v^v^v");
		Debug.Log(message);
	}

	private void LogResult(string shouldEqual, object actual)
	{
		Log(string.Format("^v^v^v SHOULD EQUAL {0} ^v^v^v", shouldEqual));
		Log(actual);
	}
}
