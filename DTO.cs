﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region DiagnosticComponentClasses

/// <summary>
/// Diagnostics data.
/// </summary>
public class DiagnosticsData
{
	public List<DiagnsoticClassData> classData;
}

/// <summary>
/// Specific class data for every diagnostic
/// </summary>
public class DiagnsoticClassData
{
	public int classID;
	public List<Vocablary> vocablary;
	public List<DiagnosticMainConcept> mainConcept;
	public List<DiagnosticDetails> details;
}

/// <summary>
/// Vocablary.
/// </summary>
public class Vocablary
{
	public string question;
	public List<string> options;
	public int rightAnswer;
}

/// <summary>
/// Diagnostic main concept.
/// </summary>
public class DiagnosticMainConcept
{
	public string passage;
	public string question;
	public List<string> options;
	public int rightAnswer;
}

/// <summary>
/// Diagnostic details.
/// </summary>
public class DiagnosticDetails
{
	public string passage;
	public List<DiagnosticQuestion> questions;
}

/// <summary>
/// Question for diagnstic detail
/// </summary>
public class DiagnosticQuestion
{
	public string question;
	public List<string> options;
	public int rightAnswer;
}

#endregion

#region EducationalComponentClasses
/// <summary>
/// Diagnostics data.
/// </summary>
public class EducationalData
{
	public int imageCount;
	public List<ClassLevelData> classData;
	public List<ReadingLevelData> readingData;
}

/// <summary>
/// Specific class data for every mainConcept
/// </summary>
public class ClassLevelData
{
	public int classID;
	public EducationalMainConcept mainConceptExample;
	public EducationalDetails detailsExample;
	public List<Levels> level;
}

public class ReadingLevelData
{
	public int readingLevelID;
	public List<Levels> level;	// will have all the reading levels from 0.1 - 13.9 considering the class range is 1 - 12
	// we don't need examples here as they will be fetched from the class level itself.
}

/// <summary>
/// Levels.
/// </summary>
public class Levels
{
    public int orderId;
	public string levelHeader;							// *****New Addition****
    public List<Topics> topics;
}


/// <summary>
/// Topics.
/// </summary>
public class Topics
{
    public int orderId;
	public string topicHeader;							// *****New Addition****
    public List<EducationalMainConcept> mainConcept;
    public List<EducationalDetails> details;
}



/// <summary>
/// Educational main concept.
/// </summary>
public class EducationalMainConcept
{
    public int orderId;
	public string header;
	public string imageURL;
	public string passage;
	public MainConceptQuestionData contentA_QuestionData;
	public MainConceptQuestionData contentB_QuestionData;
}

/// <summary>
/// Main concept question data.
/// </summary>
public class MainConceptQuestionData
{
	public string question;
	public List<Index> closeIndexes;
	public List<Index> rightIndexes;
	public List<string> options;
	public int rightAnswer;
	public int closeAnswer;
}

/// <summary>
/// Educational details.
/// </summary>
public class EducationalDetails
{
    public int orderId;
	public string header;
	public string imageURL;
	public string passage;
    public List<DetailsQuestions> detailsQuestions;
}

/// <summary>
/// A and B level Questions for details
/// </summary>
public class DetailsQuestions
{
    public DetailQuestionData contentA_QuestionData;
    public DetailQuestionData contentB_QuestionData;
}

/// <summary>
/// Questions for educational detail
/// </summary>
public class DetailQuestionData
{
	public string question;
	public List<string> options;
	public int rightAnswer;
	public List<Index> indexes;
}

/// <summary>
/// Indexes
/// </summary>
public class Index
{
	public int leftLimit;
	public int rightLimit;
}
#endregion


#region Table Of Contents 

public class TableOfContentsData
{
	public List<TocClassData> classData;
}

public class TocClassData
{
	public int classID;
	public List<TocLevelData> levelData;
}

public class TocLevelData
{
	public int levelID;
	public bool isCompleted;
	public List<TocTopicData> topicData;
}

public class TocTopicData
{
	public int topicID;
	public bool isCompleted;
} 

#endregion